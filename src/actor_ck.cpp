#include "actor_ck.h"

namespace Ckom
{

class Actor_Impl_Ck : public Actor_Ck
{
public:
    Actor_Impl_Ck(Ckom::Engine_Ck* eng)
    {
        engine = eng;
        rect.h = engine->get_window_height();
        rect.w = engine->get_window_width();
        rect.x = 0;
        rect.y = 0;
    };
    virtual ~Actor_Impl_Ck() {}

    void moove(float on_x, float on_y) override
    {
        rect.x += on_x;
        rect.y += on_y;
    }

    // void render() override final { engine->render_vp(rect); }

private:
    Ckom::Engine_Ck* engine;
    SDL_Rect         rect;
};

// Actor_Ck* create_actor(Ckom::Engine_Ck* engine)
//{
//         return new Actor_Impl_Ck(engine);
// }

void destroy_actor(Actor_Ck* actor)
{
    delete actor;
    actor = nullptr;
}

Actor_Ck::Actor_Ck() {}

} // namespace Ckom
