#include "camera_ck.h"

Camera_Ck::Camera_Ck(glm::vec3 position, glm::vec3 up, float yaw, float pitch)
    : front_(glm::vec3(0.0f, 0.0f, -1.0f))
    , position_(position)
    , world_up_(up)
    , yaw_(yaw)
    , pitch_(pitch)
{
    update_camera_vectors();
}

Camera_Ck::Camera_Ck(float pos_x, float pos_y, float pos_z, float up_x,
                     float up_y, float up_z, float yaw, float pitch)
    : position_(glm::vec3(pos_x, pos_y, pos_z))
    , world_up_(glm::vec3(up_x, up_y, up_z))
    , yaw_(yaw)
    , pitch_(pitch)
{
    update_camera_vectors();
}

glm::mat4 Camera_Ck::get_view_matrix()
{
    return glm::lookAt(position_, position_ + front_, up_);
}

void Camera_Ck::keybord_input(Movement_event direction, float delta_time)
{
    float velocity = movement_speed * delta_time;
    if (direction == Movement_event::up_pressed)
        position_ += front_ * velocity;
    if (direction == Movement_event::down_pressed)
        position_ -= front_ * velocity;
    if (direction == Movement_event::right_pressed)
        position_ += right_ * velocity;
    if (direction == Movement_event::left_pressed)
        position_ -= right_ * velocity;
}

void Camera_Ck::mouse_scroll_process(float offset_y)
{
    if (zoom >= 1.0f && zoom <= 45.0f)
        zoom -= offset_y;
    if (zoom <= 1.0f)
        zoom = 1.0f;
    if (zoom >= 45.0f)
        zoom = 45.0f;
}

void Camera_Ck::mouse_move_input(float offset_x, float offset_y,
                                 GLboolean constrain_pitch)
{
    offset_x *= mouse_sensitivity;
    offset_y *= mouse_sensitivity;

    yaw_ += offset_x;
    pitch_ += offset_y;

    if (constrain_pitch)
    {
        if (pitch_ > 89.0f)
            pitch_ = 89.0f;
        if (pitch_ < -89.0f)
            pitch_ = -89.0f;
    }
    update_camera_vectors();
}

float Camera_Ck::get_zoom() const
{
    return zoom;
}

void Camera_Ck::set_zoom(float newZoom)
{
    zoom = newZoom;
}

float Camera_Ck::get_mouse_sensitivity() const
{
    return mouse_sensitivity;
}

void Camera_Ck::set_mouse_sensitivity(float newMouse_sensitivity)
{
    mouse_sensitivity = newMouse_sensitivity;
}

float Camera_Ck::get_movement_speed() const
{
    return movement_speed;
}

void Camera_Ck::set_movement_speed(float newMovement_speed)
{
    movement_speed = newMovement_speed;
}

void Camera_Ck::update_camera_vectors()
{
    glm::vec3 front;
    front.x = std::cos(glm::radians(yaw_)) * std::cos(glm::radians(pitch_));
    front.y = std::sin(glm::radians(pitch_));
    front.z = std::sin(glm::radians(yaw_)) * std::cos(glm::radians(pitch_));
    front_  = glm::normalize(front);

    right_ = glm::normalize(glm::cross(front_, world_up_));
    up_    = glm::normalize(glm::cross(right_, front_));
}
