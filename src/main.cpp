//#include "actor_ck.h"
//#include "commands.h"
#include "engine_ck.h"

#include <iostream>

// block command pattern

// end block command pattern
// transfer from main

int main()
{
    Ckom::Engine_Ck* engine = new Ckom::Engine_Ck;
    std::string      error  = engine->initiate_engine();
    if (!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    while (!glfwWindowShouldClose(engine->getWindow()))
    {

        engine->render();
        engine->swap_buffer();
    }

    engine->uninitiate();

    delete engine;
    engine = nullptr;

    return EXIT_SUCCESS;
}

// end main
