#include "math_ck.h"

#include <assert.h>
#include <math.h>

vector_4::vector_4()
    : x(0.0f)
    , y(0.0f)
    , z(0.0f)
    , w(1.0)
{
}

vector_4::vector_4(float x_, float y_, float z_, float w_)
    : x(x_)
    , y(y_)
    , z(z_)
    , w(w_)
{
}

float vector_4::length() const
{
    return std::sqrt(x * x + y * y + z * z);
}

bool operator==(const vector_4& left, const vector_4& right)
{
    vector_4 diff = left + vector_4(-right.x, -right.y, -right.z);
    return diff.length() <= 0.000001f;
}

vector_4 operator+(const vector_4& left, const vector_4& right)
{
    vector_4 resault;
    resault.x = left.x + right.x;
    resault.y = left.y + right.y;
    resault.z = left.z + right.z;
    return resault;
}

vector_4 operator*(const vector_4& vec, const float& fl)
{
    vector_4 resault;
    resault.x = vec.x * fl;
    resault.y = vec.y * fl;
    resault.z = vec.z * fl;
    return resault;
}

vector_4 operator+(const vector_4& vec, const float& fl)
{
    vector_4 resault;
    resault.x = vec.x + fl;
    resault.y = vec.y + fl;
    resault.z = vec.z + fl;
    return resault;
}
// end vector

// MATRIX BLOCK

matrix_4x4::matrix_4x4()
    : colon_0(1.f, 0.f, 0.f, 0.f)
    , colon_1(0.f, 1.f, 0.f, 0.f)
    , colon_2(0.f, 0.f, 1.f, 0.f)
    , delta(0.f, 0.f, 0.f, 1.f)
{
}

matrix_4x4 matrix_4x4::identiry()
{
    return matrix_4x4::scale(1.0f);
}

matrix_4x4 matrix_4x4::scale(float scale_rate)
{
    matrix_4x4 resault;
    resault.colon_0.x = scale_rate;
    resault.colon_1.y = scale_rate;
    resault.colon_2.z = scale_rate;
    return resault;
}

matrix_4x4 matrix_4x4::scale(float scale_x, float scale_y, float scale_z)
{
    matrix_4x4 resault;
    resault.colon_0.x = scale_x;
    resault.colon_1.y = scale_y;
    resault.colon_2.z = scale_z;
    return resault;
}

// matrix_4x4 matrix_4x4::rotation(float angle, vector_4& vector)
//{
//     float const cosinus = std::cos(angle);
//     float const sinus   = std::sin(angle);
// }

matrix_4x4 matrix_4x4::rotation_x(float theta)
{
    matrix_4x4 resault;
    resault.colon_1.y = std::cos(theta);
    resault.colon_1.z = std::sin(theta);

    resault.colon_2.y = -std::sin(theta);
    resault.colon_2.z = std::cos(theta);
    return resault;
}

matrix_4x4 matrix_4x4::rotation_y(float theta)
{
    matrix_4x4 resault;
    resault.colon_0.x = std::cos(theta);
    resault.colon_0.z = -std::sin(theta);

    resault.colon_2.x = std::sin(theta);
    resault.colon_2.z = std::cos(theta);
    return resault;
}

matrix_4x4 matrix_4x4::rotation_z(float theta)
{
    matrix_4x4 resault;
    resault.colon_0.x = std::cos(theta);
    resault.colon_0.y = std::sin(theta);

    resault.colon_1.x = -std::sin(theta);
    resault.colon_1.y = std::cos(theta);
    return resault;
}

matrix_4x4 matrix_4x4::move(vector_4& delta)
{
    matrix_4x4 resault = matrix_4x4::identiry();
    resault.delta.x    = delta.x;
    resault.delta.y    = delta.y;
    resault.delta.z    = delta.z;
    return resault;
}

matrix_4x4 matrix_4x4::perspective(float fovy, float aspect, float near, float far)
{
    matrix_4x4 resault;

    assert(std::abs(aspect - std::numeric_limits<float>::epsilon()) > static_cast<float>(0));

    float const tan_half_fovy = std::tan(fovy / static_cast<float>(2));

    resault.colon_0.x = 1.0f / (aspect * tan_half_fovy);
    resault.colon_1.y = 1.0f / (tan_half_fovy);
    resault.colon_2.z = far / (near - far);
    resault.colon_2.w = -1.0f;
    resault.delta.z   = -(far * near) / (far - near);
    resault.delta.w   = 0;
    return resault;
}

vector_4 operator*(const matrix_4x4& matrix, const vector_4& vec)
{
    vector_4 resault;
    resault.x = matrix.colon_0.x * vec.x + matrix.colon_1.x * vec.y + matrix.colon_2.x * vec.z +
                matrix.delta.x * vec.w;
    resault.y = matrix.colon_0.y * vec.x + matrix.colon_1.y * vec.y + matrix.colon_2.y * vec.z +
                matrix.delta.y * vec.w;
    resault.z = matrix.colon_0.z * vec.x + matrix.colon_1.z * vec.y + matrix.colon_2.z * vec.z +
                matrix.delta.z * vec.w;
    resault.w = matrix.colon_0.w * vec.x + matrix.colon_1.w * vec.y + matrix.colon_2.w * vec.w +
                matrix.delta.w * vec.w;
    return resault;
}

matrix_4x4 operator*(const matrix_4x4& left_m, const matrix_4x4& right_m)
{
    matrix_4x4 resault;
    resault.colon_0.x = left_m.colon_0.x * right_m.colon_0.x +
                        left_m.colon_1.x * right_m.colon_0.y +
                        left_m.colon_2.x * right_m.colon_0.z + left_m.delta.x * right_m.colon_0.w;
    resault.colon_0.y = left_m.colon_0.y * right_m.colon_0.x +
                        left_m.colon_1.y * right_m.colon_0.y +
                        left_m.colon_2.y * right_m.colon_0.z + left_m.delta.y * right_m.colon_0.w;
    resault.colon_0.z = left_m.colon_0.z * right_m.colon_0.x +
                        left_m.colon_1.z * right_m.colon_0.y +
                        left_m.colon_2.z * right_m.colon_0.z + left_m.delta.z * right_m.colon_0.w;
    resault.colon_0.w = left_m.colon_0.w * right_m.colon_0.x +
                        left_m.colon_1.w * right_m.colon_0.y +
                        left_m.colon_2.w * right_m.colon_0.z + left_m.delta.w * right_m.colon_0.w;

    resault.colon_1.x = left_m.colon_0.x * right_m.colon_1.x +
                        left_m.colon_1.x * right_m.colon_1.y +
                        left_m.colon_2.x * right_m.colon_1.z + left_m.delta.x * right_m.colon_1.w;
    resault.colon_1.y = left_m.colon_0.y * right_m.colon_1.x +
                        left_m.colon_1.y * right_m.colon_1.y +
                        left_m.colon_2.y * right_m.colon_1.z + left_m.delta.y * right_m.colon_1.w;
    resault.colon_1.z = left_m.colon_0.z * right_m.colon_1.x +
                        left_m.colon_1.z * right_m.colon_1.y +
                        left_m.colon_2.z * right_m.colon_1.z + left_m.delta.z * right_m.colon_1.w;
    resault.colon_1.w = left_m.colon_0.w * right_m.colon_1.x +
                        left_m.colon_1.w * right_m.colon_1.y +
                        left_m.colon_2.w * right_m.colon_1.z + left_m.delta.w * right_m.colon_1.w;

    resault.colon_2.x = left_m.colon_0.x * right_m.colon_2.x +
                        left_m.colon_1.x * right_m.colon_2.y +
                        left_m.colon_2.x * right_m.colon_2.z + left_m.delta.x * right_m.colon_2.w;
    resault.colon_2.y = left_m.colon_0.y * right_m.colon_2.x +
                        left_m.colon_1.y * right_m.colon_2.y +
                        left_m.colon_2.y * right_m.colon_2.z + left_m.delta.y * right_m.colon_2.w;
    resault.colon_2.z = left_m.colon_0.z * right_m.colon_2.x +
                        left_m.colon_1.z * right_m.colon_2.y +
                        left_m.colon_2.z * right_m.colon_2.z + left_m.delta.z * right_m.colon_2.w;
    resault.colon_2.w = left_m.colon_0.w * right_m.colon_2.x +
                        left_m.colon_1.w * right_m.colon_2.y +
                        left_m.colon_2.w * right_m.colon_2.z + left_m.delta.w * right_m.colon_2.w;

    resault.delta.x = left_m.colon_0.x * right_m.delta.x + left_m.colon_1.x * right_m.delta.y +
                      left_m.colon_2.x * right_m.delta.z + left_m.delta.x * right_m.delta.w;
    resault.delta.y = left_m.colon_0.y * right_m.delta.x + left_m.colon_1.y * right_m.delta.y +
                      left_m.colon_2.y * right_m.delta.z + left_m.delta.y * right_m.delta.w;
    resault.delta.z = left_m.colon_0.z * right_m.delta.x + left_m.colon_1.z * right_m.delta.y +
                      left_m.colon_2.z * right_m.delta.z + left_m.delta.z * right_m.delta.w;
    resault.delta.w = left_m.colon_0.w * right_m.delta.x + left_m.colon_1.w * right_m.delta.y +
                      left_m.colon_2.w * right_m.delta.z + left_m.delta.w * right_m.delta.w;

    return resault;
}

// end matrix

// COLOR BLOCK

Color::Color(std::uint32_t rgba_)
    : rgba(rgba_)
{
}

Color::Color(float red, float green, float blue, float alpha)
{
    assert(red <= 1 && red >= 0);
    assert(green <= 1 && green >= 0);
    assert(blue <= 1 && blue >= 0);
    assert(alpha <= 1 && alpha >= 0);

    std::uint32_t red_   = static_cast<std::uint32_t>(red * 255);
    std::uint32_t green_ = static_cast<std::uint32_t>(green * 255);
    std::uint32_t blue_  = static_cast<std::uint32_t>(blue * 255);
    std::uint32_t alpha_ = static_cast<std::uint32_t>(alpha * 255);

    rgba = alpha_ << 24 | blue_ << 16 | green_ << 8 | red_;
}

float Color::get_red() const
{
    std::uint32_t red_ = (rgba & 0x000000FF) >> 0;
    return red_ / 255.f;
}

float Color::get_green() const
{
    std::uint32_t green_ = (rgba & 0x0000FF00) >> 8;
    return green_ / 255.f;
}

float Color::get_blue() const
{
    std::uint32_t blue_ = (rgba & 0x00FF0000) >> 16;
    return blue_ / 255.f;
}

float Color::get_alpha() const
{
    std::uint32_t alpha_ = (rgba & 0xFF000000) >> 24;
    return alpha_ / 255.f;
}

void Color::set_red(const float red)
{
    std::uint32_t red_ = static_cast<std::uint32_t>(red * 255);
    rgba &= 0xFFFFFF00;
    rgba |= (red_ << 0);
}

void Color::set_green(const float green)
{
    std::uint32_t green_ = static_cast<std::uint32_t>(green * 255);
    rgba &= 0xFFFF00FF;
    rgba |= (green_ << 8);
}

void Color::set_blue(const float blue)
{
    std::uint32_t blue_ = static_cast<std::uint32_t>(blue * 255);
    rgba &= 0xFF00FFFF;
    rgba |= (blue_ << 16);
}

void Color::set_alpha(const float alpha)
{
    std::uint32_t alpha_ = static_cast<std::uint32_t>(alpha * 255);
    rgba &= 0x00FFFFFF;
    rgba |= (alpha_ << 0);
}

// end Color Block

Triangle_pos::Triangle_pos() {}
