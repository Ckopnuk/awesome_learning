#include "commands.h"

class Go_Left_Command : public Command_Ck
{
public:
    Go_Left_Command(Ckom::Actor_Ck* act) { actor = act; }
    void execute() override { actor->moove(-speed, 0); }

private:
    Ckom::Actor_Ck* actor;
};

class Go_Right_Command : public Command_Ck
{
public:
    Go_Right_Command(Ckom::Actor_Ck* act) { actor = act; }
    void execute() override { actor->moove(speed, 0); }

private:
    Ckom::Actor_Ck* actor;
};

class Go_Up_Command : public Command_Ck
{
public:
    Go_Up_Command(Ckom::Actor_Ck* act) { actor = act; }
    void execute() override { actor->moove(0, -speed); }

private:
    Ckom::Actor_Ck* actor;
};

class Go_Down_Command : public Command_Ck
{
public:
    Go_Down_Command(Ckom::Actor_Ck* act) { actor = act; }
    void execute() override { actor->moove(0, speed); }

private:
    Ckom::Actor_Ck* actor;
};

class ESC_Command : public Command_Ck
{
public:
    ESC_Command(bool* contin_loop) { continue_loop = contin_loop; }
    void execute() override { *continue_loop = false; }

private:
    bool* continue_loop;
};

class Blackout_up : public Command_Ck
{
public:
    Blackout_up(float& v)
        : f(&v)
    {
    }
    void execute() override
    {
        if (*f <= 1)
        {
            *f += 0.02;
        }
    }

private:
    float* f;
};

void Input_Handler::set_button_left(Command_Ck* btn_l)
{
    button_left = btn_l;
}
void Input_Handler::set_button_right(Command_Ck* btn_r)
{
    button_right = btn_r;
}

Input_Handler::Input_Handler(Ckom::Actor_Ck* actor_, bool& loop, float* fl)
{
    actor         = actor_;
    continue_loop = &loop;
    change_float  = fl;
    this->init_control();
}

Input_Handler::~Input_Handler()
{
    delete button_left;
    delete button_right;
    delete button_down;
    delete button_up;
    delete button_esc;

    button_right = nullptr;
    button_left  = nullptr;
    button_down  = nullptr;
    button_up    = nullptr;
    button_esc   = nullptr;
}

// Command_Ck* Input_Handler::handle_input()
//{
//     if (engine->is_key_down(Ckom::Keys::left))
//         return button_left;
//     else if (engine->is_key_down(Ckom::Keys::right))
//         return button_right;
//     else if (engine->is_key_down(Ckom::Keys::down))
//         return button_down;
//     else if (engine->is_key_down(Ckom::Keys::up))
//         return button_up;
//     else if (engine->is_key_down(Ckom::Keys::esc))
//         return button_esc;
//     else
//         return nullptr;
//}

void Input_Handler::init_control()
{
    button_left  = new Go_Left_Command(actor);
    button_right = new Go_Right_Command(actor);
    //    button_up    = new Go_Up_Command(actor);
    button_up   = new Blackout_up(*change_float);
    button_down = new Go_Down_Command(actor);
    button_esc  = new ESC_Command(continue_loop);
}
