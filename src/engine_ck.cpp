#include "engine_ck.h"
//#include "OpenGL_loader.h"
#include "stb_image.h"

#include <SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>
#include <SDL_syswm.h>
#include <cassert>

#include <array>

#include <SDL2/SDL_opengl.h>
#include <SDL_opengl_glext.h>
#include <SDL_syswm.h>

#define CKOM_GL_CHECK()                                                        \
    {                                                                          \
        const GLenum err = glGetError();                                       \
        if (err != GL_NO_ERROR)                                                \
        {                                                                      \
            switch (err)                                                       \
            {                                                                  \
                case GL_INVALID_ENUM:                                          \
                    std::cerr << "GL_INVALID_ENUM" << std::endl;               \
                    break;                                                     \
                case GL_INVALID_VALUE:                                         \
                    std::cerr << "GL_INVALID_VALUE" << std::endl;              \
                    break;                                                     \
                case GL_INVALID_OPERATION:                                     \
                    std::cerr << "GL_INVALID_OPERATION" << std::endl;          \
                    break;                                                     \
                case GL_INVALID_FRAMEBUFFER_OPERATION:                         \
                    std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION"            \
                              << std::endl;                                    \
                    break;                                                     \
                case GL_OUT_OF_MEMORY:                                         \
                    std::cerr << "GL_OUT_OF_MEMORY" << std::endl;              \
                    break;                                                     \
            }                                                                  \
            std::cerr << __FILE__ << ':' << __LINE__ << '(' << __FUNCTION__    \
                      << ')' << std::endl;                                     \
            assert(false);                                                     \
        }                                                                      \
    }

namespace Ckom
{

// YES they ara global variables :)
bool        first_mouse = true;
float       last_x      = 640.0f / 2.0f;
float       last_y      = 480.0f / 2.0f;
float       delta_time  = 0.f;
float       last_frame  = 0.f;
Camera_Ck   camera_01(glm::vec3(0.0f, 0.0f, 3.0f));
const float pi = std::numbers::pi_v<float>;

float corner_to_radians(float angle)
{
    return (angle * pi) / 180;
}

float catang(float x)
{
    return (std::cos(x) / std::sin(x));
}

// bind block
// struct Ck_bind_keys
//{
//    Ck_bind_keys(SDL_KeyCode kc, std::string_view str, Event press, Event
//    releas, Keys ck_k)
//        : key_code(kc)
//        , key_name(str)
//        , event_press(press)
//        , event_releas(releas)
//        , ck_keys(ck_k)
//    {
//    }

//    SDL_KeyCode      key_code;
//    std::string_view key_name;
//    Event            event_press;
//    Event            event_releas;
//    Ckom::Keys       ck_keys;
//};

// const std::array<Ck_bind_keys, 5> key_arr{
//     { Ck_bind_keys{ SDLK_LEFT, "left", Event::let_pressed,
//     Event::left_released, Keys::left },
//       Ck_bind_keys{ SDLK_RIGHT, "right", Event::rigtht_pressed,
//       Event::right_released,
//                     Keys::right },
//       Ck_bind_keys{ SDLK_UP, "up", Event::up_pressed, Event::up_released,
//       Keys::up }, Ck_bind_keys{ SDLK_DOWN, "down", Event::down_pressed,
//       Event::down_released, Keys::down }, Ck_bind_keys{ SDLK_ESCAPE, "esc",
//       Event::esc_pressed, Event::esc_relesed, Keys::esc } }
// };

// bool check_input(const SDL_Event& event, const Ck_bind_keys*& result)
//{
//     const auto it = std::find_if(std::begin(key_arr), std::end(key_arr),
//                                  [&](const Ck_bind_keys bind)
//                                  { return bind.key_code ==
//                                  event.key.keysym.sym; });
//     if (it != std::end(key_arr))
//     {
//         result = &(*it);
//         return true;
//     }
//     return false;
// }

// end bind block

// VECTOR BLOCK

// ENGINE BLOCK

Engine_Ck::Engine_Ck() {}

std::string Engine_Ck::initiate_engine()
{
    std::stringstream serr; // variable to output errors

    SDL_version compiled = { 0, 0, 0 };
    SDL_version link     = { 0, 0, 0 };

    SDL_VERSION(&compiled);
    SDL_GetVersion(&link);

    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(link.major, link.minor, link.patch))
    {
        serr << "WARNING: SDL2 compiled and linked version mismatch"
             << &compiled << ' ' << &link << std::endl;
    }

    // glfwInit

    if (!glfwInit())
    {
        const char* err_msg = SDL_GetError();
        serr << "ERROR: Failed call glfwInit: " << err_msg << std::endl;
        glfwTerminate();
        return serr.str();
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // WINDOW init
    window = glfwCreateWindow(window_width, window_height, "learnOpenGL",
                              nullptr, nullptr);
    glfwMakeContextCurrent(window);
    if (!window)
    {
        const char* err_msg = SDL_GetError();
        serr << "INIT ERROR: failed call glfwCreateWindow: " << err_msg
             << std::endl;
        glfwTerminate();
        return serr.str();
    }

    glfwMakeContextCurrent(window);

    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    glfwSetInputMode(window, GLFW_CURSOR,
                     GLFW_CURSOR_DISABLED); // cursor capture

    // GLAD
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        const char* err_msg = SDL_GetError();
        serr << "INIT ERROR: failed gladLoad: " << err_msg << std::endl;
        return serr.str();
    }

    // Configuring the global state of OpenGL
    glEnable(GL_DEPTH_TEST);

    glViewport(0, 0, window_width, window_height);
    CKOM_GL_CHECK()

    shader_00 = new Shader_GL_ES(R"(
    attribute vec3 a_position;
    attribute vec2 a_tex_coord;

    varying vec2 tex_coord;
    varying vec3 v_color;

    uniform mat4 model;
    uniform mat4 view;
    uniform mat4 projection;

    void main()
    {
        tex_coord = a_tex_coord;
        gl_Position = projection * view * model * vec4(a_position,  1.0);
    }
    )",
                                 R"(
    #ifdef GL_ES
    precision mediump float;
    #endif
    varying vec3 v_color;
    varying vec2 tex_coord;
    uniform float u_color;
    uniform sampler2D texture_1;
    uniform sampler2D texture_2;
    void main()
    {
        gl_FragColor = mix(texture2D(texture_1, tex_coord),texture2D(texture_2, tex_coord), u_color);
    }
    )",
                                 { { 0, "a_position" }, { 1, "a_tex_coord" } });
    shader_00->use();
    shader_00->set_uniform("u_color", 0.2);

    matrix_4x4 projection_m;
    projection_m =
        matrix_4x4::perspective(corner_to_radians(camera_01.get_zoom()),
                                window_width / window_height, 0.1f, 100.f);
    //    glm::mat4 projection;
    //    projection = glm::perspective(glm::radians(45.0f),
    //                                  window_width / window_height, 0.1f,
    //                                  100.0f);

    shader_00->set_uniform("projection", projection_m);

    // free shaders
    //    glDeleteShader(vertex_shader);
    //    glDeleteShader(fragment_shader);

    float boxes[] = {
        -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, //
        0.5f,  -0.5f, -0.5f, 1.0f, 0.0f, //
        0.5f,  0.5f,  -0.5f, 1.0f, 1.0f, //
        0.5f,  0.5f,  -0.5f, 1.0f, 1.0f, //
        -0.5f, 0.5f,  -0.5f, 0.0f, 1.0f, //
        -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, //

        -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, //
        0.5f,  -0.5f, 0.5f,  1.0f, 0.0f, //
        0.5f,  0.5f,  0.5f,  1.0f, 1.0f, //
        0.5f,  0.5f,  0.5f,  1.0f, 1.0f, //
        -0.5f, 0.5f,  0.5f,  0.0f, 1.0f, //
        -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, //

        -0.5f, 0.5f,  0.5f,  1.0f, 0.0f, //
        -0.5f, 0.5f,  -0.5f, 1.0f, 1.0f, //
        -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, //
        -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, //
        -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, //
        -0.5f, 0.5f,  0.5f,  1.0f, 0.0f, //

        0.5f,  0.5f,  0.5f,  1.0f, 0.0f, //
        0.5f,  0.5f,  -0.5f, 1.0f, 1.0f, //
        0.5f,  -0.5f, -0.5f, 0.0f, 1.0f, //
        0.5f,  -0.5f, -0.5f, 0.0f, 1.0f, //
        0.5f,  -0.5f, 0.5f,  0.0f, 0.0f, //
        0.5f,  0.5f,  0.5f,  1.0f, 0.0f, //

        -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, //
        0.5f,  -0.5f, -0.5f, 1.0f, 1.0f, //
        0.5f,  -0.5f, 0.5f,  1.0f, 0.0f, //
        0.5f,  -0.5f, 0.5f,  1.0f, 0.0f, //
        -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, //
        -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, //

        -0.5f, 0.5f,  -0.5f, 0.0f, 1.0f, //
        0.5f,  0.5f,  -0.5f, 1.0f, 1.0f, //
        0.5f,  0.5f,  0.5f,  1.0f, 0.0f, //
        0.5f,  0.5f,  0.5f,  1.0f, 0.0f, //
        -0.5f, 0.5f,  0.5f,  0.0f, 0.0f, //
        -0.5f, 0.5f,  -0.5f, 0.0f, 1.0f  //
    };

    unsigned int indexes[] = {
        0, 1, 3, //
        1, 2, 3  //
    };

    gen_VAO_and_VBO();
    bind_VAO_and_VBO();

    glBufferData(GL_ARRAY_BUFFER, sizeof(boxes), boxes, GL_STATIC_DRAW);
    CKOM_GL_CHECK()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    CKOM_GL_CHECK()
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indexes), indexes,
                 GL_STATIC_DRAW);
    CKOM_GL_CHECK()

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
                          (void*)0); // position
    CKOM_GL_CHECK()
    glEnableVertexAttribArray(0);
    CKOM_GL_CHECK()
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
                          (void*)(3 * sizeof(float))); // color
    CKOM_GL_CHECK()
    glEnableVertexAttribArray(1);
    CKOM_GL_CHECK()
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
                          (void*)(3 * sizeof(float))); // texture
    CKOM_GL_CHECK()
    glEnableVertexAttribArray(1);
    CKOM_GL_CHECK()

    glEnable(GL_DEPTH_TEST);
    CKOM_GL_CHECK()

    texture_00 = new Texture_GL_ES("box.jpg", Extension::JPG_FILE);
    texture_01 = new Texture_GL_ES("face.png", Extension::PNG_FILE);

    shader_00->set_uniform("texture_1", texture_00);
    shader_00->set_uniform("texture_2", texture_01);

    //    glBindVertexArray(VAOs[1]);
    //    CKOM_GL_CHECK()
    //    glBindBuffer(GL_ARRAY_BUFFER, VBOs[1]);
    //    CKOM_GL_CHECK()
    //    glBufferData(GL_ARRAY_BUFFER, sizeof(second_tr), second_tr,
    //    GL_STATIC_DRAW); CKOM_GL_CHECK()
    //    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float),
    //                          (void*)0);
    //    CKOM_GL_CHECK()
    //    glEnableVertexAttribArray(0);
    //    CKOM_GL_CHECK()

    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    CKOM_GL_CHECK()

    // free buffer
    //    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //    CKOM_GL_CHECK()
    //    glBindVertexArray(0);
    //    CKOM_GL_CHECK()

    //    shader_01 = new Shader_GL_ES(
    //        R"(
    //    attribute vec2 a_position;
    //    attribute vec2 a_tex_coord;
    //    attribute vec4 a_color;

    //    uniform mat4 u_matrix;

    //    varying vec4 v_color;
    //    varying vec2 v_tex_coord;

    //    void main()
    //    {
    //        v_tex_coord = a_tex_coord;
    //        v_color = a_color;
    //        vec4 pos = u_matrix * vec4(a_position, 1.0, 1.0);
    //        gl_Position = pos;
    //    }
    //)",
    //        R"(
    //    #ifdef GL_ES
    //    precision mediump float;
    //    #endif
    //    varying vec2 v_tex_coord;
    //    varying vec4 v_color;

    //    uniform sampler2D s_texture;
    //    void main()
    //    {
    //        gl_FragColor = texture2D(s_texture, v_tex_coord) * v_color;
    //    }

    //)",
    //        { { 0, "a_position" }, { 1, "a_color" }, { 2, "a_tex_coord" } });

    //    shader_01->use();

    // init IMG
    int flags = IMG_INIT_PNG;
    if (!(IMG_Init(flags) & flags))
    {
        const char* err_msg = IMG_GetError();
        serr << "INIT ERROR: failed init SDL_img: " << err_msg << std::endl;
        return serr.str();
    }

    //    screen = SDL_GetWindowSurface(window);

    return "";
}

void Engine_Ck::uninitiate()
{

    delete shader_00;
    shader_00 = nullptr;
    delete shader_01;
    shader_01 = nullptr;
    delete texture_00;
    texture_00 = nullptr;
    SDL_GL_DeleteContext(gl_context);
    gl_context = nullptr;
    //    SDL_DestroyWindow(window);
    glfwTerminate();
    window = nullptr;
    SDL_Quit();
    IMG_Quit();
    glfwTerminate();
}

// bool Engine_Ck::read_event(Event& ev)
//{

//    SDL_Event sdl_event;
//    glfwPollEvents();

//    const Ck_bind_keys* binding = nullptr;
//    if (SDL_PollEvent(&sdl_event))
//    {
//        if (sdl_event.type == SDL_QUIT)
//        {
//            ev = Event::turn_off;
//            return true;
//        }
//        else if (sdl_event.type == SDL_KEYDOWN)
//        {
//            if (check_input(sdl_event, binding))
//            {
//                ev = binding->event_press;
//                return true;
//            }
//        }
//        else if (sdl_event.type == SDL_KEYUP)
//        {
//            if (check_input(sdl_event, binding))
//            {
//                ev = binding->event_releas;
//                return true;
//            }
//        }
//    }
//    return false;
//}

// bool Engine_Ck::is_key_down(const enum Keys key)
//{
//     const auto it = std::find_if(std::begin(key_arr), std::end(key_arr),
//                                  [&](const Ck_bind_keys& bind) { return
//                                  bind.ck_keys == key;
//                                  });
//     if (it != std::end(key_arr))
//     {
//         const std::uint8_t* state         = SDL_GetKeyboardState(nullptr);
//         int                 sdl_scan_code =
//         SDL_GetScancodeFromKey(it->key_code); return state[sdl_scan_code];
//     }
//     return false;
//}

// void Engine_Ck::render(const Triangle_pos& tr)
//{
//     shader_00->use();

//    //    // position
//    glGenVertexArrays(1, &VAO);
//    CKOM_GL_CHECK()
//    glGenBuffers(1, &VBO);
//    CKOM_GL_CHECK()
//    glBindVertexArray(VAO);
//    CKOM_GL_CHECK()
//    glBindBuffer(GL_ARRAY_BUFFER, VBO);
//    CKOM_GL_CHECK()
//    glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(Vertex_pos), &tr.vertexes[0].pos,
//    GL_STATIC_DRAW); CKOM_GL_CHECK()

//    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex_pos),
//    &tr.vertexes[0].pos); CKOM_GL_CHECK() glEnableVertexAttribArray(0);
//    CKOM_GL_CHECK()

//    glDrawArrays(GL_TRIANGLES, 0, 3);
//    CKOM_GL_CHECK()
//}

void Engine_Ck::render()
{
    shader_00->use();

    glm::mat4 view = camera_01.get_view_matrix();

    glm::vec3 cube_pos[] = {
        glm::vec3(0.0f, 0.0f, 0.0f),    glm::vec3(2.0f, 5.0f, -15.0f),
        glm::vec3(-1.5f, -2.2f, -2.5f), glm::vec3(-3.8f, -2.0f, -12.3f),
        glm::vec3(2.4f, -0.4f, -3.5f),  glm::vec3(-1.7f, 3.0f, -7.5f),
        glm::vec3(1.3f, -2.0f, -2.5f),  glm::vec3(1.5f, 2.0f, -2.5f),
        glm::vec3(1.5f, 0.2f, -1.5f),   glm::vec3(-1.3f, 1.0f, -1.5f)
    };

    for (unsigned int i = 0; i < 10; ++i)
    {
        glm::mat4 model = glm::mat4(1.0f);
        model           = glm::translate(model, cube_pos[i]);

        float angle = 25.0f * (i + 1);
        model       = glm::rotate(model, timer.elapsed() * glm::radians(angle),
                                  glm::vec3(0.5f, 1.f, 0.f));

        shader_00->set_uniform("u_color", 0.5);
        shader_00->set_uniform("model", model);
        shader_00->set_uniform("view", view);

        glBindVertexArray(VAO);
        CKOM_GL_CHECK()
        glDrawArrays(GL_TRIANGLES, 0, 36);
        CKOM_GL_CHECK()

        process_input(window);
    }
}

void Engine_Ck::swap_buffer()
{
    glfwSwapBuffers(window);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    CKOM_GL_CHECK()
}

int Engine_Ck::get_window_height() const
{
    return window_height;
}

GLFWwindow* Engine_Ck::getWindow() const
{
    return window;
}

void Engine_Ck::gen_VAO_and_VBO()
{
    glGenVertexArrays(2, &VAO);
    CKOM_GL_CHECK()
    glGenBuffers(2, &VBO);
    CKOM_GL_CHECK()
    glGenBuffers(1, &EBO);
    CKOM_GL_CHECK()
}

void Engine_Ck::bind_VAO_and_VBO()
{
    glBindVertexArray(VAO);
    CKOM_GL_CHECK()
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    CKOM_GL_CHECK()
}

int Engine_Ck::get_window_width() const
{
    return window_width;
}

void framebuffer_size_callback(GLFWwindow* window_, int width, int height)
{
    glViewport(0, 0, width, height);
}

void process_input(GLFWwindow* window)
{

    float current_frame = glfwGetTime();
    delta_time          = current_frame - last_frame;
    last_frame          = current_frame;

    glfwPollEvents();
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        camera_01.keybord_input(Movement_event::up_pressed, delta_time);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        camera_01.keybord_input(Movement_event::down_pressed, delta_time);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        camera_01.keybord_input(Movement_event::left_pressed, delta_time);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        camera_01.keybord_input(Movement_event::right_pressed, delta_time);
    }
}

// END ENGINE BLOCK!!!!

Shader_GL_ES::Shader_GL_ES(
    std::string_view vertex_shader_src, std::string_view fragmrnt_shader_src,
    const std::vector<std::tuple<GLuint, const char*>>& atrib)
{
    vert_shader = compiled_shader(GL_VERTEX_SHADER, vertex_shader_src);
    frag_shader = compiled_shader(GL_FRAGMENT_SHADER, fragmrnt_shader_src);
    if (vert_shader == 0 || frag_shader == 0)
    {
        throw std::runtime_error("can't compile shader");
    }
    programm_id = link_programm(atrib);
    if (programm_id == 0)
    {
        throw std::runtime_error("can't link programm");
    }
}

Shader_GL_ES::~Shader_GL_ES()
{
    glDeleteShader(vert_shader);
    glDeleteShader(frag_shader);
    glDeleteProgram(programm_id);
}

void Shader_GL_ES::use() const
{
    glUseProgram(programm_id);
    CKOM_GL_CHECK()
}

void Shader_GL_ES::set_uniform(std::string_view uniform_name,
                               Texture_GL_ES*   texture)
{
    assert(texture != nullptr);
    const int location = glGetUniformLocation(programm_id, uniform_name.data());
    CKOM_GL_CHECK()
    if (location == -1)
    {
        std::cerr << "Can't get uniform location\n";
        std::runtime_error("Text uniform location error");
    }

    static unsigned int texture_unit = 0;
    glActiveTexture(GL_TEXTURE0 + texture_unit);
    CKOM_GL_CHECK()
    texture->bind();
    glUniform1i(location, static_cast<int>(0 + texture_unit));
    CKOM_GL_CHECK()
    texture_unit++;
}

void Shader_GL_ES::set_uniform(std::string_view uniform_name,
                               const Color&     color)
{
    const int location = glGetUniformLocation(programm_id, uniform_name.data());
    CKOM_GL_CHECK()
    if (location == -1)
    {
        std::cerr << "Can't get uniform location from shader\n";
        std::runtime_error("Can't get uniform location!");
    }
    float values[4] = { color.get_red(), color.get_green(), color.get_blue(),
                        color.get_alpha() };
    glUniform4fv(location, 1, &values[0]);
    CKOM_GL_CHECK()
}

void Shader_GL_ES::set_uniform(std::string_view uniform_name, const float value)
{
    const int location = glGetUniformLocation(programm_id, uniform_name.data());
    CKOM_GL_CHECK()
    if (location == -1)
    {
        std::cerr << "Cant't get float uniform location\n";
        std::runtime_error("can't get float iniform location\n");
    }
    glUniform1f(location, value);
    CKOM_GL_CHECK()
}

void Shader_GL_ES::set_uniform(std::string_view uniform_name,
                               const matrix_4x4 matrix)
{
    const int location = glGetUniformLocation(programm_id, uniform_name.data());
    CKOM_GL_CHECK()
    if (location == -1)
    {
        std::cerr << "can't get matrix uniform locetion\n";
        std::runtime_error("can't get matrix uniform locetion\n");
    }

    // clang-format off
    float values[16] = {
        matrix.colon_0.x, matrix.colon_0.y, matrix.colon_0.z, matrix.colon_0.w,
        matrix.colon_1.x, matrix.colon_1.y, matrix.colon_1.z, matrix.colon_1.w,
        matrix.colon_2.x, matrix.colon_2.y, matrix.colon_2.z, matrix.colon_2.w,
        matrix.delta.x,   matrix.delta.y,   matrix.delta.z,   matrix.delta.w
    };
    // clang-format on

    glUniformMatrix4fv(location, 1, GL_FALSE, &values[0]);
    CKOM_GL_CHECK()
}

void Shader_GL_ES::set_uniform(std::string_view uniform_name,
                               const glm::mat4  matrix)
{
    const int location = glGetUniformLocation(programm_id, uniform_name.data());
    CKOM_GL_CHECK()
    if (location == -1)
    {
        std::cerr << "can't get matrix uniform locetion\n";
        std::runtime_error("can't get matrix uniform locetion\n");
    }

    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
    CKOM_GL_CHECK()
}

GLuint Shader_GL_ES::compiled_shader(GLenum           shader_type,
                                     std::string_view shader_src)
{

    GLuint shader_id = glCreateShader(shader_type);
    CKOM_GL_CHECK()
    const char* source = shader_src.data();
    glShaderSource(shader_id, 1, &source, nullptr);
    CKOM_GL_CHECK()
    glCompileShader(shader_id);
    CKOM_GL_CHECK()

    GLint compiled_status = 0;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compiled_status);
    CKOM_GL_CHECK()
    if (compiled_status == 0)
    {
        GLint info_len = 0;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_len);
        CKOM_GL_CHECK()
        std::vector<char> info_chars(static_cast<size_t>(info_len));
        glGetShaderInfoLog(shader_id, info_len, nullptr, info_chars.data());
        CKOM_GL_CHECK()
        glDeleteShader(shader_id);
        CKOM_GL_CHECK()

        std::string shader_type_name =
            shader_type == GL_VERTEX_SHADER ? "vertex" : "fragment";
        std::cerr << "ERROR compiled" << shader_type << " shader/n"
                  << shader_src.data() << "/n" << info_chars.data();
        return 0;
    }
    return shader_id;
}

GLuint Shader_GL_ES::link_programm(
    const std::vector<std::tuple<GLuint, const char*>>& atrib)
{
    GLuint shader_programm_id = glCreateProgram();
    CKOM_GL_CHECK()
    if (shader_programm_id == 0)
    {
        std::cerr << "failed create programm ";
        throw std::runtime_error("can't link shader");
    }
    glAttachShader(shader_programm_id, vert_shader);
    CKOM_GL_CHECK()
    glAttachShader(shader_programm_id, frag_shader);
    CKOM_GL_CHECK()

    for (const auto& attr : atrib)
    {
        GLuint        loc  = std::get<0>(attr);
        const GLchar* name = std::get<1>(attr);
        glBindAttribLocation(shader_programm_id, loc, name);
        CKOM_GL_CHECK()
    }

    glLinkProgram(shader_programm_id);
    CKOM_GL_CHECK()

    GLint link_status = 0;
    glGetProgramiv(shader_programm_id, GL_LINK_STATUS, &link_status);
    CKOM_GL_CHECK()
    if (link_status == 0)
    {
        GLint info_len = 0;
        glGetProgramiv(shader_programm_id, GL_INFO_LOG_LENGTH, &info_len);
        CKOM_GL_CHECK()
        std::vector<char> info_log(static_cast<size_t>(info_len));
        glGetProgramInfoLog(shader_programm_id, info_len, nullptr,
                            info_log.data());
        CKOM_GL_CHECK()
        std::cerr << "ERROR link programm:/n" << info_log.data();
        glDeleteProgram(shader_programm_id);
        CKOM_GL_CHECK()
        return 0;
    }
    return shader_programm_id;
}

GLuint Shader_GL_ES::get_programm_id() const
{
    return programm_id;
}

Texture_GL_ES::Texture_GL_ES(std::string_view path, Extension file_extension)
    : file_path(path)
{

    glGenTextures(1, &texture);
    CKOM_GL_CHECK()
    glBindTexture(GL_TEXTURE_2D, texture);
    CKOM_GL_CHECK()

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    CKOM_GL_CHECK()
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    CKOM_GL_CHECK()
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    CKOM_GL_CHECK()
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    CKOM_GL_CHECK()

    stbi_set_flip_vertically_on_load(true);

    unsigned char* data =
        stbi_load(path.data(), &width, &height, &nr_chanels, 0);
    if (data)
    {
        if (file_extension == Extension::PNG_FILE)
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA,
                         GL_UNSIGNED_BYTE, data);
            CKOM_GL_CHECK()
        }
        else
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
                         GL_UNSIGNED_BYTE, data);
            CKOM_GL_CHECK()
        }
        glGenerateMipmap(GL_TEXTURE_2D);
        CKOM_GL_CHECK()
    }
    else
    {
        std::cerr << "Failed to load texture " << file_path.data() << std::endl;
    }
    stbi_image_free(data);
}

Texture_GL_ES::Texture_GL_ES(const void* pixels, const size_t width_,
                             const size_t height_)
    : width(width_)
    , height(height_)
{
    glGenTextures(1, &texture);
    CKOM_GL_CHECK()
    glBindTexture(GL_TEXTURE_2D, texture);
    CKOM_GL_CHECK()

    GLint   border = 0;
    GLsizei w      = static_cast<GLsizei>(width);
    GLsizei h      = static_cast<GLsizei>(height);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, border, GL_RGBA,
                 GL_UNSIGNED_BYTE, pixels);
    CKOM_GL_CHECK()

    glGenerateMipmap(GL_TEXTURE_2D);
    CKOM_GL_CHECK()

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    CKOM_GL_CHECK()
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    CKOM_GL_CHECK()

    if (file_path.empty())
    {
        file_path = "::memory::";
    }
}

Texture_GL_ES::~Texture_GL_ES()
{
    glDeleteTextures(1, &texture);
}

void Texture_GL_ES::bind() const
{
    GLboolean is_texture = glIsTexture(texture);
    CKOM_GL_CHECK()
    assert(is_texture);
    // it was
    glBindTexture(GL_TEXTURE_2D, texture);
    CKOM_GL_CHECK()
}

Vertex_buffer::Vertex_buffer(const Triangle_pct* tri, std::size_t n)
    : count(static_cast<std::uint32_t>(n * 3))
{
    glGenBuffers(1, &gl_handle);
    CKOM_GL_CHECK()

    bind();
    GLsizeiptr size_in_bytes = n * 3 * sizeof(Vertex_pct);
    glBufferData(GL_ARRAY_BUFFER, size_in_bytes, &tri->vert[0], GL_STATIC_DRAW);
    CKOM_GL_CHECK()
}

Vertex_buffer::Vertex_buffer(const Vertex_pct* vert, std::size_t n)
    : count(static_cast<std::uint32_t>(n))
{
    glGenBuffers(1, &gl_handle);
    CKOM_GL_CHECK()

    bind();
    GLsizeiptr size_in_bytes = static_cast<GLsizeiptr>(n * sizeof(Vertex_pct));
    glBufferData(GL_ARRAY_BUFFER, size_in_bytes, vert, GL_STATIC_DRAW);
    CKOM_GL_CHECK()
}

void Vertex_buffer::bind() const
{
    glBindBuffer(GL_ARRAY_BUFFER, gl_handle);
    CKOM_GL_CHECK()
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if (first_mouse)
    {
        last_x      = xpos;
        last_y      = ypos;
        first_mouse = false;
    }

    float x_offset = xpos - last_x;
    float y_offset = last_y - ypos;

    last_x = xpos;
    last_y = ypos;

    camera_01.mouse_move_input(x_offset, y_offset);
}

void scroll_callback(GLFWwindow* window, double x_offset, double y_offset)
{
    camera_01.mouse_scroll_process(y_offset);
}

} // namespace Ckom
