cmake_minimum_required(VERSION 3.20)

project(learning_OpenGL)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)

set(eng_version engine_01.1)

#include_directories(${PROJECT_SOURCE_DIR}/glad/include/glad)
include_directories(${eng_version} PRIVATE include/glad)

add_library(${eng_version} STATIC include/engine_ck.h
                                  src/engine_ck.cpp
                                  include/commands.h
                                  src/commands.cpp
                                  include/actor_ck.h
                                  src/actor_ck.cpp
                                  include/OpenGL_loader.h
                                  include/stb_image.h
                                  src/stb_image.cpp
                                  include/Timer.h
                                  include/glad/glad.h
                                  include/KHR/khrplatform.h
                                  src/glad.c
                                  include/math_ck.h
                                  src/math_ck.cpp
                                  include/camera_ck.h
                                  src/camera_ck.cpp
                                  )

find_package(SDL2 REQUIRED)
find_package(glfw3 3.3 REQUIRED)

INCLUDE(FindPkgConfig)

PKG_SEARCH_MODULE(SDL2 REQUIRED sdl2)
PKG_SEARCH_MODULE(SDL2IMAGE REQUIRED SDL2_image>=2.0.0)


if(WIN32)
      target_compile_definitions(${engine-version} PRIVATE "-DCKOM_DECLSPEC=__declspec(dllexport)")
endif(WIN32)

target_include_directories(${eng_version} PRIVATE ${SDL2_INCLUDE_DIRS} ${SDL2IMAGE_INCLUDE_DIRS})
target_link_libraries(${eng_version} PRIVATE ${SDL2_LIBRARIES} ${SDL2IMAGE_LIBRARIES})

include_directories(${eng_version} PRIVATE include)


target_link_libraries(${eng_version} PRIVATE glfw)

if (MINGW)
    target_link_libraries(${eng_version} PRIVATE
        -lmingw32
        -lSDL2main
        -lSDL2
        -mwindows
        -lopengl32
        )

elseif(APPLE)
    target_link_libraries(${eng_version} PRIVATE
        -Wl,-framework,OpenGL
        )
elseif(UNIX)
    target_link_libraries(${eng_version} PRIVATE -lSDL2 -lGL)


elseif(MSVC)
target_link_libraries(${eng-version} PRIVATE SDL2::SDL2 SDL2::SDL2main opengl32)
endif()

#INCLUDE_DIRECTORIES(${SDL2_INCLUDE_DIRS} ${SDL2IMAGE_INCLUDE_DIRS})
#TARGET_LINK_LIBRARIES(${eng_version} ${SDL2_LIBRARIES} ${SDL2IMAGE_LIBRARIES})


find_library(SDL2_LIB NAMES SDL2)

add_executable(${PROJECT_NAME}  src/main.cpp)

target_link_libraries(${PROJECT_NAME} ${eng_version})
