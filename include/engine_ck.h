#ifndef ENGINE_CK_H
#define ENGINE_CK_H

#include "Timer.h"

#include <iosfwd>
//#include <iostream>
#include <sstream>
#include <string>
#include <string_view>
#include <tuple>
#include <vector>

#include <variant>

#include "camera_ck.h"
#include "math_ck.h"

#include "glad/glad.h"
#include <glm/glm.hpp>

#include <GLFW/glfw3.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <SDL2/SDL.h>

#ifndef CKOM_DECLSPEC
#define CKOM_DECLSPEC
#endif

namespace Ckom
{

// enum for keys bind
enum class Keys
{
    left,
    right,
    up,
    down,
    esc
};

enum class Extension
{
    PNG_FILE,
    JPG_FILE
};

class Shader_GL_ES;
class Texture_GL_ES;
class Vertex_buffer;

class CKOM_DECLSPEC Engine_Ck
{
public:
    Engine_Ck();
    std::string initiate_engine();
    void        uninitiate();

    //    bool read_event(Event& ev);
    //    bool is_key_down(const Keys key);

    //    void render(const Triangle_pos& tr);
    void render();
    //    void render(const Vertex_buffer& buff, Texture_GL_ES* texture);

    void swap_buffer();

    int get_window_width() const;
    int get_window_height() const;

    GLFWwindow* getWindow() const;

private:
    void gen_VAO_and_VBO();
    void bind_VAO_and_VBO();

    const float window_width  = 640;
    const float window_height = 480;

    GLFWwindow*    window     = nullptr;
    SDL_GLContext* gl_context = nullptr;
    Shader_GL_ES*  shader_00  = nullptr;
    Shader_GL_ES*  shader_01  = nullptr;
    Texture_GL_ES* texture_00 = nullptr;
    Texture_GL_ES* texture_01 = nullptr;

    //    Camera_Ck* camera_00 = nullptr;

    Timer timer;

    uint32_t VBOs[2], VAOs[2];
    uint32_t VBO = 0;
    uint32_t VAO = 0;
    uint32_t EBO = 0;

    GLuint shader_program = 0; // to shader programm
};

// CKOM_DECLSPEC Engine_Ck* create_engine_ck();
// CKOM_DECLSPEC void       destroy_engine_ck(Engine_Ck* engine);

class CKOM_DECLSPEC Texture_GL_ES
{
public:
    explicit Texture_GL_ES(std::string_view path, Extension file_extension);
    Texture_GL_ES(const void* pixels, const size_t width_,
                  const size_t height_);
    ~Texture_GL_ES();
    void bind() const;

    std::uint32_t get_width() const { return width; }
    std::uint32_t get_height() const { return height; }
    std::string   get_name() const { return file_path; }

    void add_ref() { ++ref_counter; }
    bool remove_ref()
    {
        --ref_counter;
        if (ref_counter == 0)
        {
            delete this;
            return true;
        }
        return false;
    }

private:
    GLuint       texture = 0;
    std::string  file_path;
    size_t       ref_counter = 1;
    std::int32_t width       = 0;
    std::int32_t height      = 0;
    std::int32_t nr_chanels  = 0;
};

class CKOM_DECLSPEC Vertex_buffer
{
public:
    Vertex_buffer(const Triangle_pct* tri, std::size_t n);
    Vertex_buffer(const Vertex_pct* vert, std::size_t n);
    ~Vertex_buffer() {}
    void   bind() const;
    size_t size() const { return count; }

private:
    std::uint32_t count{ 0 };
    std::uint32_t gl_handle{ 0 };
};

class CKOM_DECLSPEC Shader_GL_ES
{
public:
    Shader_GL_ES(std::string_view vertex_shader_src,
                 std::string_view fragmrnt_shader_src,
                 const std::vector<std::tuple<GLuint, const char*>>& atrib);
    ~Shader_GL_ES();
    void use() const;
    void set_uniform(std::string_view uniform_name, Texture_GL_ES* texture);
    void set_uniform(std::string_view uniform_name, const Color& color);
    void set_uniform(std::string_view uniform_name, const float value);
    void set_uniform(std::string_view uniform_name, const matrix_4x4 matrix);
    void set_uniform(std::string_view uniform_name, const glm::mat4 matrix);

    GLuint get_programm_id() const;

private:
    GLuint compiled_shader(GLenum shader_type, std::string_view shader_src);
    GLuint link_programm(
        const std::vector<std::tuple<GLuint, const char*>>& atrib);
    GLuint   vert_shader = 0;
    GLuint   frag_shader = 0;
    GLuint   programm_id = 0;
    uint32_t default_vbo = 0;
};

void process_input(GLFWwindow* window);
void framebuffer_size_callback(GLFWwindow* window_, int width, int height);
void scroll_callback(GLFWwindow* window, double x_offset, double y_offset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);

} // namespace Ckom

#endif // ENGINE_CK_H
