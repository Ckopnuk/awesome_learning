#ifndef OPENGL_LOADER_H
#define OPENGL_LOADER_H

#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_opengl_glext.h>
#include <SDL2/SDL_syswm.h>

#include <iostream>

static PFNGLCREATESHADERPROC      glCreateShader      = nullptr;
static PFNGLSHADERSOURCEPROC      glShaderSource      = nullptr;
static PFNGLCOMPILESHADERPROC     glCompileShader     = nullptr;
static PFNGLGETSHADERIVPROC       glGetShaderiv       = nullptr;
static PFNGLGETSHADERINFOLOGPROC  glGetShaderInfoLog  = nullptr;
static PFNGLCREATEPROGRAMPROC     glCreateProgram     = nullptr;
static PFNGLATTACHSHADERPROC      glAttachShader      = nullptr;
static PFNGLLINKPROGRAMPROC       glLinkProgram       = nullptr;
static PFNGLGETPROGRAMIVPROC      glGetProgramiv      = nullptr;
static PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = nullptr;
static PFNGLDELETESHADERPROC      glDeleteShader      = nullptr;
static PFNGLBUFFERSUBDATAPROC     glBufferSubData     = nullptr;

static PFNGLGENVERTEXARRAYSPROC         glGenVertexArrays         = nullptr;
static PFNGLGENBUFFERSPROC              glGenBuffers              = nullptr;
static PFNGLBINDVERTEXARRAYPROC         glBindVertexArray         = nullptr;
static PFNGLBINDBUFFERPROC              glBindBuffer              = nullptr;
static PFNGLBUFFERDATAPROC              glBufferData              = nullptr;
static PFNGLVERTEXATTRIBPOINTERPROC     glVertexAttribPointer     = nullptr;
static PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = nullptr;
static PFNGLUSEPROGRAMPROC              glUseProgram              = nullptr;
static PFNGLBINDATTRIBLOCATIONPROC      glBindAttribLocation      = nullptr;
static PFNGLDELETEPROGRAMPROC           glDeleteProgram           = nullptr;
static PFNGLGETUNIFORMLOCATIONPROC      glGetUniformLocation      = nullptr;

static PFNGLUNIFORM1FPROC        glUniform1f        = nullptr;
static PFNGLUNIFORM4FPROC        glUniform4f        = nullptr;
static PFNGLUNIFORM4FVPROC       glUniform4fv       = nullptr;
static PFNGLUNIFORM1IPROC        glUniform1i        = nullptr;
static PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv = nullptr;

static PFNGLGENERATEMIPMAPPROC glGenerateMipmap  = nullptr;
static PFNGLACTIVETEXTUREPROC  glActiveTextureMY = nullptr;

template <typename T>
static void load_gl_function(const char* funcName, T& result)
{
    void* gl_pointer = SDL_GL_GetProcAddress(funcName);
    if (gl_pointer == nullptr)
    {
        throw std::runtime_error(std::string("can't load gl function ") + funcName);
    }
    result = reinterpret_cast<T>(gl_pointer);
}

static const char* load_all_gl_function()
{
    try
    {
        load_gl_function("glCreateShader", glCreateShader);
        load_gl_function("glShaderSource", glShaderSource);
        load_gl_function("glCompileShader", glCompileShader);
        load_gl_function("glGetShaderiv", glGetShaderiv);
        load_gl_function("glGetShaderInfoLog", glGetShaderInfoLog);
        load_gl_function("glCreateProgram", glCreateProgram);
        load_gl_function("glAttachShader", glAttachShader);
        load_gl_function("glLinkProgram", glLinkProgram);
        load_gl_function("glGetProgramiv", glGetProgramiv);
        load_gl_function("glGetProgramInfoLog", glGetProgramInfoLog);
        load_gl_function("glDeleteShader", glDeleteShader);
        load_gl_function("glGenVertexArrays", glGenVertexArrays);
        load_gl_function("glGenBuffers", glGenBuffers);
        load_gl_function("glBindVertexArray", glBindVertexArray);
        load_gl_function("glBindBuffer", glBindBuffer);
        load_gl_function("glBufferData", glBufferData);
        load_gl_function("glVertexAttribPointer", glVertexAttribPointer);
        load_gl_function("glUseProgram", glUseProgram);
        load_gl_function("glEnableVertexAttribArray", glEnableVertexAttribArray);
        load_gl_function("glBindAttribLocation", glBindAttribLocation);
        load_gl_function("glDeleteProgram", glDeleteProgram);
        load_gl_function("glBufferSubData", glBufferSubData);
        load_gl_function("glGetUniformLocation", glGetUniformLocation);

        load_gl_function("glUniform4f", glUniform4f);
        load_gl_function("glUniform4fv", glUniform4fv);
        load_gl_function("glUniform1f", glUniform1f);
        load_gl_function("glUniformMatrix4fv", glUniformMatrix4fv);

        load_gl_function("glGenerateMipmap", glGenerateMipmap);
        //        load_gl_function("glActiveTexture", glActiveTexture);
        load_gl_function("glUniform1i", glUniform1i);
    }
    catch (std::exception& ex)
    {
        return ex.what();
    }
    return "";
}

#endif // OPENGL_LOADER_H
