#ifndef MATH_CK_H
#define MATH_CK_H

#ifndef CKOM_DECLSPEC
#define CKOM_DECLSPEC
#endif

#include <iostream>

struct CKOM_DECLSPEC vector_4
{
    vector_4();
    vector_4(const vector_4&) = default;
    vector_4(float x_, float y_, float z_ = 0.f, float w_ = 1.f);
    float length() const;
    float x = 0.f;
    float y = 0.f;
    float z = 0.f;
    float w = 1.f;
};

CKOM_DECLSPEC bool     operator==(const vector_4& left, const vector_4& right);
CKOM_DECLSPEC vector_4 operator+(const vector_4& left, const vector_4& right);
CKOM_DECLSPEC vector_4 operator*(const vector_4& vec, const float& fl);
CKOM_DECLSPEC vector_4 operator+(const vector_4& vec, const float& fl);

struct CKOM_DECLSPEC matrix_4x4
{
    matrix_4x4();
    static matrix_4x4 identiry();
    static matrix_4x4 scale(float scale_rate);
    static matrix_4x4 scale(float scale_x, float scale_y, float scale_z = 1.0f);
    //    static matrix_4x4 rotation(float angle, vector_4& vect/or);
    static matrix_4x4 rotation_x(float theta);
    static matrix_4x4 rotation_y(float theta);
    static matrix_4x4 rotation_z(float theta);
    static matrix_4x4 move(vector_4& delta);
    static matrix_4x4 perspective(float fovy, float aspect, float near, float far);
    vector_4          colon_0;
    vector_4          colon_1;
    vector_4          colon_2;
    vector_4          delta;
};

CKOM_DECLSPEC vector_4   operator*(const matrix_4x4& matrix, const vector_4& vec);
CKOM_DECLSPEC matrix_4x4 operator*(const matrix_4x4& left_m, const matrix_4x4& right_m);

class CKOM_DECLSPEC Color
{
public:
    Color() = default;
    explicit Color(std::uint32_t rgba_);
    Color(float red, float green, float blue, float alpha);

    float get_red() const;
    float get_green() const;
    float get_blue() const;
    float get_alpha() const;

    void set_red(const float red);
    void set_green(const float green);
    void set_blue(const float blue);
    void set_alpha(const float alpha);

private:
    std::uint32_t rgba;
};

struct CKOM_DECLSPEC Vertex_pos
{
    vector_4 pos;
};

struct CKOM_DECLSPEC Vertex_pct
{
    vector_4 pos;
    vector_4 tex;
    Color    col;
};

struct CKOM_DECLSPEC Triangle_pos
{
    Triangle_pos();
    Vertex_pos vertexes[3];
};

struct CKOM_DECLSPEC Triangle_pct
{
    Triangle_pct();
    Vertex_pct vert[3];
};

#endif // MATH_CK_H
