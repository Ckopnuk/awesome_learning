#ifndef ACTOR_CK_H
#define ACTOR_CK_H

#include "engine_ck.h"

namespace Ckom
{
class Actor_Ck
{
public:
    Actor_Ck();
    virtual ~Actor_Ck();
    virtual void moove(float on_x, float on_y);

    virtual void render() = 0;

private:
    Ckom::Engine_Ck* engine;
    SDL_Rect         rect;
};

inline Actor_Ck::~Actor_Ck() {}

inline void Actor_Ck::moove(float on_x, float on_y) {}

// Actor_Ck* create_actor(Engine_Ck* engine);
void destroy_actor(Actor_Ck* actor);

} // namespace Ckom

#endif // ACTOR_CK_H
