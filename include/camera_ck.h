#ifndef CAMERA_CK_H
#define CAMERA_CK_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// controll event enum
enum class Movement_event
{
    left_pressed,
    left_released,

    right_pressed,
    right_released,

    up_pressed,
    up_released,

    down_pressed,
    down_released
};

class Camera_Ck
{
public:
    Camera_Ck(glm::vec3 position, glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f),
              float yaw = -90.f, float pitch = 0.0f);
    Camera_Ck(float pos_x, float pos_y, float pos_z, float up_x, float up_y,
              float up_z, float yaw, float pitch);

    glm::mat4 get_view_matrix();
    void      keybord_input(Movement_event direction, float delta_time);
    void      mouse_scroll_process(float offset_y);
    void      mouse_move_input(float offset_x, float offset_y,
                               GLboolean constrain_pitch = true);

    float get_zoom() const;
    void  set_zoom(float new_zoom);
    float get_mouse_sensitivity() const;
    void  set_mouse_sensitivity(float newMouse_sensitivity);
    float get_movement_speed() const;
    void  set_movement_speed(float newMovement_speed);

private:
    // camera attributes
    glm::vec3 position_;
    glm::vec3 front_;
    glm::vec3 up_;
    glm::vec3 right_;
    glm::vec3 world_up_;

    // euler angles
    float yaw_   = -90.0f;
    float pitch_ = 0.0f;

    // camera settings
    float movement_speed    = 2.5f;
    float mouse_sensitivity = 0.1f;
    float zoom              = 45.0f;

    // calculates a cvector-forward
    void update_camera_vectors();
};

#endif // CAMERA_CK_H
