#ifndef COMMANDS_H
#define COMMANDS_H

#include "actor_ck.h"

class Command_Ck
{
public:
    Command_Ck() {}
    virtual ~Command_Ck() {}
    virtual void set_speed(float speed_) { speed = speed_; }
    virtual void execute() = 0;

protected:
    float speed = 10;
};

class Input_Handler
{
public:
    Input_Handler(Ckom::Actor_Ck* actor_, bool& loop, float* fl);
    ~Input_Handler();
    //    Command_Ck* handle_input();
    void set_button_left(Command_Ck* btn_l);
    void set_button_right(Command_Ck* btn_r);
    void init_control();

private:
    Ckom::Actor_Ck*  actor;
    Command_Ck*      button_left;
    Command_Ck*      button_right;
    Command_Ck*      button_up;
    Command_Ck*      button_down;
    Command_Ck*      button_esc;
    Ckom::Engine_Ck* engine;
    bool*            continue_loop;
    float*           change_float;
};

#endif // COMMANDS_H
