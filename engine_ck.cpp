#include "header/engine_ck.h"
#include "header/OpenGL_loader.h"
#include "header/stb_image.h"

#include <SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>
#include <SDL_syswm.h>
#include <cassert>

#include <array>

#include <SDL2/SDL_opengl.h>
#include <SDL_opengl_glext.h>
#include <SDL_syswm.h>

#define CKOM_GL_CHECK()                                                                            \
    {                                                                                              \
        const GLenum err = glGetError();                                                           \
        if (err != GL_NO_ERROR)                                                                    \
        {                                                                                          \
            switch (err)                                                                           \
            {                                                                                      \
                case GL_INVALID_ENUM:                                                              \
                    std::cerr << "GL_INVALID_ENUM" << std::endl;                                   \
                    break;                                                                         \
                case GL_INVALID_VALUE:                                                             \
                    std::cerr << "GL_INVALID_VALUE" << std::endl;                                  \
                    break;                                                                         \
                case GL_INVALID_OPERATION:                                                         \
                    std::cerr << "GL_INVALID_OPERATION" << std::endl;                              \
                    break;                                                                         \
                case GL_INVALID_FRAMEBUFFER_OPERATION:                                             \
                    std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION" << std::endl;                  \
                    break;                                                                         \
                case GL_OUT_OF_MEMORY:                                                             \
                    std::cerr << "GL_OUT_OF_MEMORY" << std::endl;                                  \
                    break;                                                                         \
            }                                                                                      \
            std::cerr << __FILE__ << ':' << __LINE__ << '(' << __FUNCTION__ << ')' << std::endl;   \
            assert(false);                                                                         \
        }                                                                                          \
    }

namespace Ckom
{

// whenever the window is resized this callback function is called
void framebuffer_size_callback(/*GLwindow* window_,*/ int width, int height); // NEED TO FIX!!!!!!

const float pi = std::numbers::pi_v<float>;

float corner_to_radians(float angle)
{
    return (angle * pi) / 180;
}

float catang(float x)
{
    return (std::cos(x) / std::sin(x));
}

// bind block
struct Ck_bind_keys
{
    Ck_bind_keys(SDL_KeyCode kc, std::string_view str, Event press, Event releas, Keys ck_k)
        : key_code(kc)
        , key_name(str)
        , event_press(press)
        , event_releas(releas)
        , ck_keys(ck_k)
    {
    }

    SDL_KeyCode      key_code;
    std::string_view key_name;
    Event            event_press;
    Event            event_releas;
    Ckom::Keys       ck_keys;
};

const std::array<Ck_bind_keys, 5> key_arr{
    { Ck_bind_keys{ SDLK_LEFT, "left", Event::let_pressed, Event::left_released, Keys::left },
      Ck_bind_keys{ SDLK_RIGHT, "right", Event::rigtht_pressed, Event::right_released,
                    Keys::right },
      Ck_bind_keys{ SDLK_UP, "up", Event::up_pressed, Event::up_released, Keys::up },
      Ck_bind_keys{ SDLK_DOWN, "down", Event::down_pressed, Event::down_released, Keys::down },
      Ck_bind_keys{ SDLK_ESCAPE, "esc", Event::esc_pressed, Event::esc_relesed, Keys::esc } }
};

bool check_input(const SDL_Event& event, const Ck_bind_keys*& result)
{
    const auto it = std::find_if(std::begin(key_arr), std::end(key_arr),
                                 [&](const Ck_bind_keys bind)
                                 { return bind.key_code == event.key.keysym.sym; });
    if (it != std::end(key_arr))
    {
        result = &(*it);
        return true;
    }
    return false;
}

// end bind block

// VECTOR BLOCK
vector_4::vector_4()
    : x(0.0f)
    , y(0.0f)
    , z(0.0f)
    , w(1.0)
{
}

vector_4::vector_4(float x_, float y_, float z_, float w_)
    : x(x_)
    , y(y_)
    , z(z_)
    , w(w_)
{
}

float vector_4::length() const
{
    return std::sqrt(x * x + y * y + z * z);
}

bool operator==(const vector_4& left, const vector_4& right)
{
    vector_4 diff = left + vector_4(-right.x, -right.y, -right.z);
    return diff.length() <= 0.000001f;
}

vector_4 operator+(const vector_4& left, const vector_4& right)
{
    vector_4 resault;
    resault.x = left.x + right.x;
    resault.y = left.y + right.y;
    resault.z = left.z + right.z;
    return resault;
}

vector_4 operator*(const vector_4& vec, const float& fl)
{
    vector_4 resault;
    resault.x = vec.x * fl;
    resault.y = vec.y * fl;
    resault.z = vec.z * fl;
    return resault;
}

vector_4 operator+(const vector_4& vec, const float& fl)
{
    vector_4 resault;
    resault.x = vec.x + fl;
    resault.y = vec.y + fl;
    resault.z = vec.z + fl;
    return resault;
}
// end vector

// MATRIX BLOCK

matrix_4x4::matrix_4x4()
    : colon_0(1.f, 0.f, 0.f, 0.f)
    , colon_1(0.f, 1.f, 0.f, 0.f)
    , colon_2(0.f, 0.f, 1.f, 0.f)
    , delta(0.f, 0.f, 0.f, 1.f)
{
}

matrix_4x4 matrix_4x4::identiry()
{
    return matrix_4x4::scale(1.0f);
}

matrix_4x4 matrix_4x4::scale(float scale_rate)
{
    matrix_4x4 resault;
    resault.colon_0.x = scale_rate;
    resault.colon_1.y = scale_rate;
    resault.colon_2.z = scale_rate;
    return resault;
}

matrix_4x4 matrix_4x4::scale(float scale_x, float scale_y, float scale_z)
{
    matrix_4x4 resault;
    resault.colon_0.x = scale_x;
    resault.colon_1.y = scale_y;
    resault.colon_2.z = scale_z;
    return resault;
}

matrix_4x4 matrix_4x4::rotation_x(float theta)
{
    matrix_4x4 resault;
    resault.colon_1.y = std::cos(theta);
    resault.colon_1.z = std::sin(theta);

    resault.colon_2.y = -std::sin(theta);
    resault.colon_2.z = std::cos(theta);
    return resault;
}

matrix_4x4 matrix_4x4::rotation_y(float theta)
{
    matrix_4x4 resault;
    resault.colon_0.x = std::cos(theta);
    resault.colon_0.z = -std::sin(theta);

    resault.colon_2.x = std::sin(theta);
    resault.colon_2.z = std::cos(theta);
    return resault;
}

matrix_4x4 matrix_4x4::rotation_z(float theta)
{
    matrix_4x4 resault;
    resault.colon_0.x = std::cos(theta);
    resault.colon_0.y = std::sin(theta);

    resault.colon_1.x = -std::sin(theta);
    resault.colon_1.y = std::cos(theta);
    return resault;
}

matrix_4x4 matrix_4x4::move(vector_4& delta)
{
    matrix_4x4 resault = matrix_4x4::identiry();
    resault.delta.x    = delta.x;
    resault.delta.y    = delta.y;
    resault.delta.z    = delta.z;
    return resault;
}

matrix_4x4 matrix_4x4::perspective(float fovy, float aspect, float near, float far)
{
    matrix_4x4 resault;
    resault.colon_0.x = (catang(fovy / 2)) / aspect;
    resault.colon_1.y = catang(fovy / 2);
    resault.colon_2.z = (far + near) / (far - near);
    resault.colon_2.w = (-2 * far * near) / (far - near);
    resault.delta.z   = 1;
    resault.delta.w   = 0;
    return resault;
}

vector_4 operator*(const matrix_4x4& matrix, const vector_4& vec)
{
    vector_4 resault;
    resault.x = matrix.colon_0.x * vec.x + matrix.colon_1.x * vec.y + matrix.colon_2.x * vec.z +
                matrix.delta.x * vec.w;
    resault.y = matrix.colon_0.y * vec.x + matrix.colon_1.y * vec.y + matrix.colon_2.y * vec.z +
                matrix.delta.y * vec.w;
    resault.z = matrix.colon_0.z * vec.x + matrix.colon_1.z * vec.y + matrix.colon_2.z * vec.z +
                matrix.delta.z * vec.w;
    resault.w = matrix.colon_0.w * vec.x + matrix.colon_1.w * vec.y + matrix.colon_2.w * vec.w +
                matrix.delta.w * vec.w;
    return resault;
}

matrix_4x4 operator*(const matrix_4x4& left_m, const matrix_4x4& right_m)
{
    matrix_4x4 resault;
    resault.colon_0.x = left_m.colon_0.x * right_m.colon_0.x +
                        left_m.colon_1.x * right_m.colon_0.y +
                        left_m.colon_2.x * right_m.colon_0.z + left_m.delta.x * right_m.colon_0.w;
    resault.colon_0.y = left_m.colon_0.y * right_m.colon_0.x +
                        left_m.colon_1.y * right_m.colon_0.y +
                        left_m.colon_2.y * right_m.colon_0.z + left_m.delta.y * right_m.colon_0.w;
    resault.colon_0.z = left_m.colon_0.z * right_m.colon_0.x +
                        left_m.colon_1.z * right_m.colon_0.y +
                        left_m.colon_2.z * right_m.colon_0.z + left_m.delta.z * right_m.colon_0.w;
    resault.colon_0.w = left_m.colon_0.w * right_m.colon_0.x +
                        left_m.colon_1.w * right_m.colon_0.y +
                        left_m.colon_2.w * right_m.colon_0.z + left_m.delta.w * right_m.colon_0.w;

    resault.colon_1.x = left_m.colon_0.x * right_m.colon_1.x +
                        left_m.colon_1.x * right_m.colon_1.y +
                        left_m.colon_2.x * right_m.colon_1.z + left_m.delta.x * right_m.colon_1.w;
    resault.colon_1.y = left_m.colon_0.y * right_m.colon_1.x +
                        left_m.colon_1.y * right_m.colon_1.y +
                        left_m.colon_2.y * right_m.colon_1.z + left_m.delta.y * right_m.colon_1.w;
    resault.colon_1.z = left_m.colon_0.z * right_m.colon_1.x +
                        left_m.colon_1.z * right_m.colon_1.y +
                        left_m.colon_2.z * right_m.colon_1.z + left_m.delta.z * right_m.colon_1.w;
    resault.colon_1.w = left_m.colon_0.w * right_m.colon_1.x +
                        left_m.colon_1.w * right_m.colon_1.y +
                        left_m.colon_2.w * right_m.colon_1.z + left_m.delta.w * right_m.colon_1.w;

    resault.colon_2.x = left_m.colon_0.x * right_m.colon_2.x +
                        left_m.colon_1.x * right_m.colon_2.y +
                        left_m.colon_2.x * right_m.colon_2.z + left_m.delta.x * right_m.colon_2.w;
    resault.colon_2.y = left_m.colon_0.y * right_m.colon_2.x +
                        left_m.colon_1.y * right_m.colon_2.y +
                        left_m.colon_2.y * right_m.colon_2.z + left_m.delta.y * right_m.colon_2.w;
    resault.colon_2.z = left_m.colon_0.z * right_m.colon_2.x +
                        left_m.colon_1.z * right_m.colon_2.y +
                        left_m.colon_2.z * right_m.colon_2.z + left_m.delta.z * right_m.colon_2.w;
    resault.colon_2.w = left_m.colon_0.w * right_m.colon_2.x +
                        left_m.colon_1.w * right_m.colon_2.y +
                        left_m.colon_2.w * right_m.colon_2.z + left_m.delta.w * right_m.colon_2.w;

    resault.delta.x = left_m.colon_0.x * right_m.delta.x + left_m.colon_1.x * right_m.delta.y +
                      left_m.colon_2.x * right_m.delta.z + left_m.delta.x * right_m.delta.w;
    resault.delta.y = left_m.colon_0.y * right_m.delta.x + left_m.colon_1.y * right_m.delta.y +
                      left_m.colon_2.y * right_m.delta.z + left_m.delta.y * right_m.delta.w;
    resault.delta.z = left_m.colon_0.z * right_m.delta.x + left_m.colon_1.z * right_m.delta.y +
                      left_m.colon_2.z * right_m.delta.z + left_m.delta.z * right_m.delta.w;
    resault.delta.w = left_m.colon_0.w * right_m.delta.x + left_m.colon_1.w * right_m.delta.y +
                      left_m.colon_2.w * right_m.delta.z + left_m.delta.w * right_m.delta.w;

    return resault;
}

// end matrix

// COLOR BLOCK

Color::Color(std::uint32_t rgba_)
    : rgba(rgba_)
{
}

Color::Color(float red, float green, float blue, float alpha)
{
    assert(red <= 1 && red >= 0);
    assert(green <= 1 && green >= 0);
    assert(blue <= 1 && blue >= 0);
    assert(alpha <= 1 && alpha >= 0);

    std::uint32_t red_   = static_cast<std::uint32_t>(red * 255);
    std::uint32_t green_ = static_cast<std::uint32_t>(green * 255);
    std::uint32_t blue_  = static_cast<std::uint32_t>(blue * 255);
    std::uint32_t alpha_ = static_cast<std::uint32_t>(alpha * 255);

    rgba = alpha_ << 24 | blue_ << 16 | green_ << 8 | red_;
}

float Color::get_red() const
{
    std::uint32_t red_ = (rgba & 0x000000FF) >> 0;
    return red_ / 255.f;
}

float Color::get_green() const
{
    std::uint32_t green_ = (rgba & 0x0000FF00) >> 8;
    return green_ / 255.f;
}

float Color::get_blue() const
{
    std::uint32_t blue_ = (rgba & 0x00FF0000) >> 16;
    return blue_ / 255.f;
}

float Color::get_alpha() const
{
    std::uint32_t alpha_ = (rgba & 0xFF000000) >> 24;
    return alpha_ / 255.f;
}

void Color::set_red(const float red)
{
    std::uint32_t red_ = static_cast<std::uint32_t>(red * 255);
    rgba &= 0xFFFFFF00;
    rgba |= (red_ << 0);
}

void Color::set_green(const float green)
{
    std::uint32_t green_ = static_cast<std::uint32_t>(green * 255);
    rgba &= 0xFFFF00FF;
    rgba |= (green_ << 8);
}

void Color::set_blue(const float blue)
{
    std::uint32_t blue_ = static_cast<std::uint32_t>(blue * 255);
    rgba &= 0xFF00FFFF;
    rgba |= (blue_ << 16);
}

void Color::set_alpha(const float alpha)
{
    std::uint32_t alpha_ = static_cast<std::uint32_t>(alpha * 255);
    rgba &= 0x00FFFFFF;
    rgba |= (alpha_ << 0);
}

// end Color Block

// ENGINE BLOCK

Engine_Ck::Engine_Ck() {}

std::string Engine_Ck::initiate_engine()
{
    std::stringstream serr; // variable to output errors

    SDL_version compiled = { 0, 0, 0 };
    SDL_version link     = { 0, 0, 0 };

    SDL_VERSION(&compiled);
    SDL_GetVersion(&link);

    if (SDL_COMPILEDVERSION != SDL_VERSIONNUM(link.major, link.minor, link.patch))
    {
        serr << "WARNING: SDL2 compiled and linked version mismatch" << &compiled << ' ' << &link
             << std::endl;
    }

    // init sdl
    const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0)
    {
        const char* err_msg = SDL_GetError();
        serr << "ERROR: Failed call SDL_Init: " << err_msg << std::endl;
        return serr.str();
    }

    // init window
    window = SDL_CreateWindow("game", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_width,
                              window_height, ::SDL_WINDOW_OPENGL);
    if (!window)
    {
        const char* err_msg = SDL_GetError();
        serr << "INIT ERROR: failed call glfwCreateWindow: " << err_msg << std::endl;
        SDL_Quit();
        return serr.str();
    }

    // configured
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

    // create context
    gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr)
    {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
        gl_context = SDL_GL_CreateContext(window);
        if (!gl_context)
        {
            const char* err_msg = SDL_GetError();
            serr << "ERROR: can't create OpenGL context: " << err_msg << std::endl;
            return serr.str();
        }
    }

    //    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    glViewport(0, 0, window_width, window_height);
    CKOM_GL_CHECK()

    std::string load_gl_function_error = load_all_gl_function();
    if (!load_gl_function_error.empty())
    {
        return load_gl_function_error;
    }

    // VERTEX SHADER compilation
    const char* vertex_shader_source = "#version 330 core\n"
                                       "layout (location = 0) in vec3 aPos;\n"
                                       "void main()\n"
                                       "{\n"
                                       "gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
                                       "}\0";

    GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    CKOM_GL_CHECK()
    glShaderSource(vertex_shader, 1, &vertex_shader_source, NULL);
    CKOM_GL_CHECK()
    glCompileShader(vertex_shader);
    CKOM_GL_CHECK()
    // Checking for vertex shader compilation errors
    GLint compiled_status;
    char  infolog[512];
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compiled_status);
    CKOM_GL_CHECK()
    if (compiled_status == 0)
    {
        glGetShaderInfoLog(vertex_shader, 512, NULL, infolog);
        CKOM_GL_CHECK()
        serr << "ERROR: shader vertex compilation failed - " << infolog << std::endl;
        return serr.str();
    }

    // FRAGMENT SHADER compilation
    const char* fragment_shader_src = "#version 330 core\n"
                                      "out vec4 FragColor;\n"
                                      "uniform vec4 u_color;\n"
                                      "void main()\n"
                                      "{\n"
                                      "FragColor = u_color;\n"
                                      "}\n\0";

    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_shader_src, NULL);
    CKOM_GL_CHECK()
    glCompileShader(fragment_shader);
    CKOM_GL_CHECK()
    // Checking for fragment shader compilation errors
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compiled_status);
    CKOM_GL_CHECK()
    if (compiled_status == 0)
    {
        glGetShaderInfoLog(fragment_shader, 512, NULL, infolog);
        CKOM_GL_CHECK()
        serr << "ERROR: fragment shader compilation failed - " << infolog << std::endl;
        return serr.str();
    }

    // LINKING SHADERS
    shader_program = glCreateProgram();
    glAttachShader(shader_program, vertex_shader);
    CKOM_GL_CHECK()
    glAttachShader(shader_program, fragment_shader);
    CKOM_GL_CHECK()
    glLinkProgram(shader_program);
    CKOM_GL_CHECK()
    // Checking for compile shader linking Errors
    glGetProgramiv(shader_program, GL_LINK_STATUS, &compiled_status);
    CKOM_GL_CHECK()
    if (compiled_status == 0)
    {
        glGetProgramInfoLog(shader_program, 512, NULL, infolog);
        CKOM_GL_CHECK()
        serr << "ERROR: link shader program failed - " << infolog << std::endl;
        return serr.str();
    }

    shader_00 = new Shader_GL_ES(R"(
    attribute vec3 a_position;
    attribute vec2 a_tex_coord;

    varying vec2 tex_coord;
    varying vec3 v_color;

    uniform mat4 model;
    uniform mat4 view;
    uniform mat4 projection;

    void main()
    {
        tex_coord = a_tex_coord;
        gl_Position = projection * view * model * vec4(a_position,  1.0);
    }
    )",
                                 R"(
    #ifdef GL_ES
    precision mediump float;
    #endif
    varying vec3 v_color;
    varying vec2 tex_coord;
    uniform float u_color;
    uniform sampler2D texture_1;
    uniform sampler2D texture_2;
    void main()
    {
        gl_FragColor = mix(texture2D(texture_1, tex_coord),texture2D(texture_2, tex_coord), u_color);
    }
    )",
                                 { { 0, "a_position" }, { 1, "a_tex_coord" } });
    shader_00->use();
    shader_00->set_uniform("u_color", 0.2);

    glm::mat4 projection;
    projection = glm::perspective(glm::radians(45.0f), window_width / window_height, 0.1f, 100.0f);

    shader_00->set_uniform("projection", projection);

    // free shaders
    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);

    ///////// TEMPORARY CODE //////////// {

    float first_tr[] = {
        -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, //
        0.5f,  -0.5f, -0.5f, 1.0f, 0.0f, //
        0.5f,  0.5f,  -0.5f, 1.0f, 1.0f, //
        0.5f,  0.5f,  -0.5f, 1.0f, 1.0f, //
        -0.5f, 0.5f,  -0.5f, 0.0f, 1.0f, //
        -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, //

        -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, //
        0.5f,  -0.5f, 0.5f,  1.0f, 0.0f, //
        0.5f,  0.5f,  0.5f,  1.0f, 1.0f, //
        0.5f,  0.5f,  0.5f,  1.0f, 1.0f, //
        -0.5f, 0.5f,  0.5f,  0.0f, 1.0f, //
        -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, //

        -0.5f, 0.5f,  0.5f,  1.0f, 0.0f, //
        -0.5f, 0.5f,  -0.5f, 1.0f, 1.0f, //
        -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, //
        -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, //
        -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, //
        -0.5f, 0.5f,  0.5f,  1.0f, 0.0f, //

        0.5f,  0.5f,  0.5f,  1.0f, 0.0f, //
        0.5f,  0.5f,  -0.5f, 1.0f, 1.0f, //
        0.5f,  -0.5f, -0.5f, 0.0f, 1.0f, //
        0.5f,  -0.5f, -0.5f, 0.0f, 1.0f, //
        0.5f,  -0.5f, 0.5f,  0.0f, 0.0f, //
        0.5f,  0.5f,  0.5f,  1.0f, 0.0f, //

        -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, //
        0.5f,  -0.5f, -0.5f, 1.0f, 1.0f, //
        0.5f,  -0.5f, 0.5f,  1.0f, 0.0f, //
        0.5f,  -0.5f, 0.5f,  1.0f, 0.0f, //
        -0.5f, -0.5f, 0.5f,  0.0f, 0.0f, //
        -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, //

        -0.5f, 0.5f,  -0.5f, 0.0f, 1.0f, //
        0.5f,  0.5f,  -0.5f, 1.0f, 1.0f, //
        0.5f,  0.5f,  0.5f,  1.0f, 0.0f, //
        0.5f,  0.5f,  0.5f,  1.0f, 0.0f, //
        -0.5f, 0.5f,  0.5f,  0.0f, 0.0f, //
        -0.5f, 0.5f,  -0.5f, 0.0f, 1.0f  //
    };

    //    float second_tr[] = {
    //        -0.5f, 0.5f,  0.0f, //
    //        -0.5f, -0.5f, 0.0f, //
    //        0.5f,  0.5f,  0.0f  //
    //    };

    unsigned int indexes[] = {
        0, 1, 3, //
        1, 2, 3  //
    };

    glGenVertexArrays(2, VAOs);
    CKOM_GL_CHECK()
    glGenBuffers(2, VBOs);
    CKOM_GL_CHECK()
    glGenBuffers(1, &EBO);
    CKOM_GL_CHECK()

    glBindVertexArray(VAOs[0]);
    CKOM_GL_CHECK()
    glBindBuffer(GL_ARRAY_BUFFER, VBOs[0]);
    CKOM_GL_CHECK()
    glBufferData(GL_ARRAY_BUFFER, sizeof(first_tr), first_tr, GL_STATIC_DRAW);
    CKOM_GL_CHECK()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    CKOM_GL_CHECK()
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indexes), indexes, GL_STATIC_DRAW);
    CKOM_GL_CHECK()

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0); // position
    CKOM_GL_CHECK()
    glEnableVertexAttribArray(0);
    CKOM_GL_CHECK()
    //    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
    //                          (void*)(3 * sizeof(float))); // color
    //    CKOM_GL_CHECK()
    //    glEnableVertexAttribArray(1);
    CKOM_GL_CHECK()
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
                          (void*)(3 * sizeof(float))); // texture
    CKOM_GL_CHECK()
    glEnableVertexAttribArray(1);
    CKOM_GL_CHECK()

    glEnable(GL_DEPTH_TEST);
    CKOM_GL_CHECK()

    texture_00 = new Texture_GL_ES("box.jpg", Extension::JPG_FILE);
    texture_01 = new Texture_GL_ES("face.png", Extension::PNG_FILE);

    shader_00->set_uniform("texture_1", texture_00);
    shader_00->set_uniform("texture_2", texture_01);

    glBindVertexArray(VAOs[1]);
    CKOM_GL_CHECK()
    glBindBuffer(GL_ARRAY_BUFFER, VBOs[1]);
    CKOM_GL_CHECK()
    //    glBufferData(GL_ARRAY_BUFFER, sizeof(second_tr), second_tr, GL_STATIC_DRAW);
    CKOM_GL_CHECK()
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    CKOM_GL_CHECK()
    glEnableVertexAttribArray(0);
    CKOM_GL_CHECK()

    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    CKOM_GL_CHECK()

    // free buffer
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    CKOM_GL_CHECK()
    glBindVertexArray(0);
    CKOM_GL_CHECK()

    shader_01 = new Shader_GL_ES(
        R"(
    attribute vec2 a_position;
    attribute vec2 a_tex_coord;
    attribute vec4 a_color;

    uniform mat4 u_matrix;

    varying vec4 v_color;
    varying vec2 v_tex_coord;

    void main()
    {
        v_tex_coord = a_tex_coord;
        v_color = a_color;
        vec4 pos = u_matrix * vec4(a_position, 1.0, 1.0);
        gl_Position = pos;
    }
)",
        R"(
    #ifdef GL_ES
    precision mediump float;
    #endif
    varying vec2 v_tex_coord;
    varying vec4 v_color;

    uniform sampler2D s_texture;
    void main()
    {
        gl_FragColor = texture2D(s_texture, v_tex_coord) * v_color;
    }

)",
        { { 0, "a_position" }, { 1, "a_color" }, { 2, "a_tex_coord" } });

    shader_01->use();

    // init IMG
    int flags = IMG_INIT_PNG;
    if (!(IMG_Init(flags) & flags))
    {
        const char* err_msg = IMG_GetError();
        serr << "INIT ERROR: failed init SDL_img: " << err_msg << std::endl;
        return serr.str();
    }

    //    screen = SDL_GetWindowSurface(window);

    return "";
}

void Engine_Ck::uninitiate()
{

    delete shader_00;
    shader_00 = nullptr;
    delete shader_01;
    shader_01 = nullptr;
    delete texture_00;
    texture_00 = nullptr;
    SDL_GL_DeleteContext(gl_context);
    gl_context = nullptr;
    SDL_DestroyWindow(window);
    window = nullptr;
    SDL_Quit();
    IMG_Quit();
}

bool Engine_Ck::read_event(Event& ev)
{

    SDL_Event sdl_event;
    //    glfwPollEvents();

    const Ck_bind_keys* binding = nullptr;
    if (SDL_PollEvent(&sdl_event))
    {
        if (sdl_event.type == SDL_QUIT)
        {
            ev = Event::turn_off;
            return true;
        }
        else if (sdl_event.type == SDL_KEYDOWN)
        {
            if (check_input(sdl_event, binding))
            {
                ev = binding->event_press;
                return true;
            }
        }
        else if (sdl_event.type == SDL_KEYUP)
        {
            if (check_input(sdl_event, binding))
            {
                ev = binding->event_releas;
                return true;
            }
        }
    }
    return false;
}

bool Engine_Ck::is_key_down(const enum Keys key)
{
    const auto it = std::find_if(std::begin(key_arr), std::end(key_arr),
                                 [&](const Ck_bind_keys& bind) { return bind.ck_keys == key; });
    if (it != std::end(key_arr))
    {
        const std::uint8_t* state         = SDL_GetKeyboardState(nullptr);
        int                 sdl_scan_code = SDL_GetScancodeFromKey(it->key_code);
        return state[sdl_scan_code];
    }
    return false;
}

void Engine_Ck::render(const Triangle_pos& tr)
{
    shader_00->use();

    //    // position
    glGenVertexArrays(1, &VAO);
    CKOM_GL_CHECK()
    glGenBuffers(1, &VBO);
    CKOM_GL_CHECK()
    glBindVertexArray(VAO);
    CKOM_GL_CHECK()
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    CKOM_GL_CHECK()
    glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(Vertex_pos), &tr.vertexes[0].pos, GL_STATIC_DRAW);
    CKOM_GL_CHECK()

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex_pos), &tr.vertexes[0].pos);
    CKOM_GL_CHECK()
    glEnableVertexAttribArray(0);
    CKOM_GL_CHECK()

    glDrawArrays(GL_TRIANGLES, 0, 3);
    CKOM_GL_CHECK()
}

void Engine_Ck::render()
{
    shader_00->use();

    glm::vec3 cube_pos[] = { glm::vec3(0.0f, 0.0f, 0.0f),    glm::vec3(2.0f, 5.0f, -15.0f),
                             glm::vec3(-1.5f, -2.2f, -2.5f), glm::vec3(-3.8f, -2.0f, -12.3f),
                             glm::vec3(2.4f, -0.4f, -3.5f),  glm::vec3(-1.7f, 3.0f, -7.5f),
                             glm::vec3(1.3f, -2.0f, -2.5f),  glm::vec3(1.5f, 2.0f, -2.5f),
                             glm::vec3(1.5f, 0.2f, -1.5f),   glm::vec3(-1.3f, 1.0f, -1.5f) };

    for (unsigned int i = 0; i < 10; ++i)
    {
        glm::mat4 model = glm::mat4(1.0f);
        model           = glm::translate(model, cube_pos[i]);
        float angle     = 25.0f * (i + 1);
        model =
            glm::rotate(model, timer.elapsed() * glm::radians(angle), glm::vec3(0.5f, 1.f, 0.f));
        glm::mat4 view = glm::mat4(1.0f);
        view           = glm::translate(view, glm::vec3(0.f, 0.f, -3.f));

        shader_00->set_uniform("u_color", 0.5);
        shader_00->set_uniform("model", model);
        shader_00->set_uniform("view", view);

        //    shader_00->set_uniform("u_matrix", matrix);
        glBindVertexArray(VAOs[0]);
        CKOM_GL_CHECK()
        //    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        CKOM_GL_CHECK()
    }
}

void Engine_Ck::swap_buffer()
{
    SDL_GL_SwapWindow(window);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    CKOM_GL_CHECK()
}

int Engine_Ck::get_window_height() const
{
    return window_height;
}

int Engine_Ck::get_window_width() const
{
    return window_width;
}

Triangle_pos::Triangle_pos() {}

// void framebuffer_size_callback(GLFWwindow* window_, int width, int height)
//{
//    glViewport(0, 0, width, height);
//}

// END ENGINE BLOCK!!!!

Shader_GL_ES::Shader_GL_ES(std::string_view vertex_shader_src, std::string_view fragmrnt_shader_src,
                           const std::vector<std::tuple<GLuint, const char*>>& atrib)
{
    vert_shader = compiled_shader(GL_VERTEX_SHADER, vertex_shader_src);
    frag_shader = compiled_shader(GL_FRAGMENT_SHADER, fragmrnt_shader_src);
    if (vert_shader == 0 || frag_shader == 0)
    {
        throw std::runtime_error("can't compile shader");
    }
    programm_id = link_programm(atrib);
    if (programm_id == 0)
    {
        throw std::runtime_error("can't link programm");
    }
}

void Shader_GL_ES::use() const
{
    glUseProgram(programm_id);
    CKOM_GL_CHECK()
}

void Shader_GL_ES::set_uniform(std::string_view uniform_name, Texture_GL_ES* texture)
{
    assert(texture != nullptr);
    const int location = glGetUniformLocation(programm_id, uniform_name.data());
    CKOM_GL_CHECK()
    if (location == -1)
    {
        std::cerr << "Can't get uniform location\n";
        std::runtime_error("Text uniform location error");
    }

    static unsigned int texture_unit = 0;
    glActiveTexture(GL_TEXTURE0 + texture_unit);
    CKOM_GL_CHECK()
    texture->bind();
    glUniform1i(location, static_cast<int>(0 + texture_unit));
    CKOM_GL_CHECK()
    texture_unit++;
}

void Shader_GL_ES::set_uniform(std::string_view uniform_name, const Color& color)
{
    const int location = glGetUniformLocation(programm_id, uniform_name.data());
    CKOM_GL_CHECK()
    if (location == -1)
    {
        std::cerr << "Can't get uniform location from shader\n";
        std::runtime_error("Can't get uniform location!");
    }
    float values[4] = { color.get_red(), color.get_green(), color.get_blue(), color.get_alpha() };
    glUniform4fv(location, 1, &values[0]);
    CKOM_GL_CHECK()
}

void Shader_GL_ES::set_uniform(std::string_view uniform_name, const float value)
{
    const int location = glGetUniformLocation(programm_id, uniform_name.data());
    CKOM_GL_CHECK()
    if (location == -1)
    {
        std::cerr << "Cant't get float uniform location\n";
        std::runtime_error("can't get float iniform location\n");
    }
    glUniform1f(location, value);
    CKOM_GL_CHECK()
}

void Shader_GL_ES::set_uniform(std::string_view uniform_name, const matrix_4x4 matrix)
{
    const int location = glGetUniformLocation(programm_id, uniform_name.data());
    CKOM_GL_CHECK()
    if (location == -1)
    {
        std::cerr << "can't get matrix uniform locetion\n";
        std::runtime_error("can't get matrix uniform locetion\n");
    }

    // clang-format off
    float values[16] = {
        matrix.colon_0.x, matrix.colon_0.y, matrix.colon_0.z, matrix.colon_0.w,
        matrix.colon_1.x, matrix.colon_1.y, matrix.colon_1.z, matrix.colon_1.w,
        matrix.colon_2.x, matrix.colon_2.y, matrix.colon_2.z, matrix.colon_2.w,
        matrix.delta.x,   matrix.delta.y,   matrix.delta.z,   matrix.delta.w
    };
    // clang-format on

    glUniformMatrix4fv(location, 1, GL_TRUE, &values[0]);
    CKOM_GL_CHECK()
}

void Shader_GL_ES::set_uniform(std::string_view uniform_name, const glm::mat4 matrix)
{
    const int location = glGetUniformLocation(programm_id, uniform_name.data());
    CKOM_GL_CHECK()
    if (location == -1)
    {
        std::cerr << "can't get matrix uniform locetion\n";
        std::runtime_error("can't get matrix uniform locetion\n");
    }

    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
    CKOM_GL_CHECK()
}

GLuint Shader_GL_ES::compiled_shader(GLenum shader_type, std::string_view shader_src)
{

    GLuint shader_id = glCreateShader(shader_type);
    CKOM_GL_CHECK()
    const char* source = shader_src.data();
    glShaderSource(shader_id, 1, &source, nullptr);
    CKOM_GL_CHECK()
    glCompileShader(shader_id);
    CKOM_GL_CHECK()

    GLint compiled_status = 0;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compiled_status);
    CKOM_GL_CHECK()
    if (compiled_status == 0)
    {
        GLint info_len = 0;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_len);
        CKOM_GL_CHECK()
        std::vector<char> info_chars(static_cast<size_t>(info_len));
        glGetShaderInfoLog(shader_id, info_len, nullptr, info_chars.data());
        CKOM_GL_CHECK()
        glDeleteShader(shader_id);
        CKOM_GL_CHECK()

        std::string shader_type_name = shader_type == GL_VERTEX_SHADER ? "vertex" : "fragment";
        std::cerr << "ERROR compiled" << shader_type << " shader/n" << shader_src.data() << "/n"
                  << info_chars.data();
        return 0;
    }
    return shader_id;
}

GLuint Shader_GL_ES::link_programm(const std::vector<std::tuple<GLuint, const char*>>& atrib)
{
    GLuint shader_programm_id = glCreateProgram();
    CKOM_GL_CHECK()
    if (shader_programm_id == 0)
    {
        std::cerr << "failed create programm ";
        throw std::runtime_error("can't link shader");
    }
    glAttachShader(shader_programm_id, vert_shader);
    CKOM_GL_CHECK()
    glAttachShader(shader_programm_id, frag_shader);
    CKOM_GL_CHECK()

    for (const auto& attr : atrib)
    {
        GLuint        loc  = std::get<0>(attr);
        const GLchar* name = std::get<1>(attr);
        glBindAttribLocation(shader_programm_id, loc, name);
        CKOM_GL_CHECK()
    }

    glLinkProgram(shader_programm_id);
    CKOM_GL_CHECK()

    GLint link_status = 0;
    glGetProgramiv(shader_programm_id, GL_LINK_STATUS, &link_status);
    CKOM_GL_CHECK()
    if (link_status == 0)
    {
        GLint info_len = 0;
        glGetProgramiv(shader_programm_id, GL_INFO_LOG_LENGTH, &info_len);
        CKOM_GL_CHECK()
        std::vector<char> info_log(static_cast<size_t>(info_len));
        glGetProgramInfoLog(shader_programm_id, info_len, nullptr, info_log.data());
        CKOM_GL_CHECK()
        std::cerr << "ERROR link programm:/n" << info_log.data();
        glDeleteProgram(shader_programm_id);
        CKOM_GL_CHECK()
        return 0;
    }
    return shader_programm_id;
}

GLuint Shader_GL_ES::get_programm_id() const
{
    return programm_id;
}

Texture_GL_ES::Texture_GL_ES(std::string_view path, Extension file_extension)
    : file_path(path)
{

    glGenTextures(1, &texture);
    CKOM_GL_CHECK()
    glBindTexture(GL_TEXTURE_2D, texture);
    CKOM_GL_CHECK()

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    CKOM_GL_CHECK()
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    CKOM_GL_CHECK()
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    CKOM_GL_CHECK()
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    CKOM_GL_CHECK()

    stbi_set_flip_vertically_on_load(true);

    unsigned char* data = stbi_load(path.data(), &width, &height, &nr_chanels, 0);
    if (data)
    {
        if (file_extension == Extension::PNG_FILE)
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE,
                         data);
            CKOM_GL_CHECK()
        }
        else
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE,
                         data);
            CKOM_GL_CHECK()
        }
        glGenerateMipmap(GL_TEXTURE_2D);
        CKOM_GL_CHECK()
    }
    else
    {
        std::cerr << "Failed to load texture " << file_path.data() << std::endl;
    }
    stbi_image_free(data);
}

Texture_GL_ES::Texture_GL_ES(const void* pixels, const size_t width_, const size_t height_)
    : width(width_)
    , height(height_)
{
    glGenTextures(1, &texture);
    CKOM_GL_CHECK()
    glBindTexture(GL_TEXTURE_2D, texture);
    CKOM_GL_CHECK()

    GLint   border = 0;
    GLsizei w      = static_cast<GLsizei>(width);
    GLsizei h      = static_cast<GLsizei>(height);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, border, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    CKOM_GL_CHECK()

    glGenerateMipmap(GL_TEXTURE_2D);
    CKOM_GL_CHECK()

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    CKOM_GL_CHECK()
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    CKOM_GL_CHECK()

    if (file_path.empty())
    {
        file_path = "::memory::";
    }
}

Texture_GL_ES::~Texture_GL_ES()
{
    glDeleteTextures(1, &texture);
}

void Texture_GL_ES::bind() const
{
    GLboolean is_texture = glIsTexture(texture);
    CKOM_GL_CHECK()
    assert(is_texture);
    // it was
    glBindTexture(GL_TEXTURE_2D, texture);
    CKOM_GL_CHECK()
}

Vertex_buffer::Vertex_buffer(const Triangle_pct* tri, std::size_t n)
    : count(static_cast<std::uint32_t>(n * 3))
{
    glGenBuffers(1, &gl_handle);
    CKOM_GL_CHECK()

    bind();
    GLsizeiptr size_in_bytes = n * 3 * sizeof(Vertex_pct);
    glBufferData(GL_ARRAY_BUFFER, size_in_bytes, &tri->vert[0], GL_STATIC_DRAW);
    CKOM_GL_CHECK()
}

Vertex_buffer::Vertex_buffer(const Vertex_pct* vert, std::size_t n)
    : count(static_cast<std::uint32_t>(n))
{
    glGenBuffers(1, &gl_handle);
    CKOM_GL_CHECK()

    bind();
    GLsizeiptr size_in_bytes = static_cast<GLsizeiptr>(n * sizeof(Vertex_pct));
    glBufferData(GL_ARRAY_BUFFER, size_in_bytes, vert, GL_STATIC_DRAW);
    CKOM_GL_CHECK()
}

void Vertex_buffer::bind() const
{
    glBindBuffer(GL_ARRAY_BUFFER, gl_handle);
    CKOM_GL_CHECK()
}

} // namespace Ckom
